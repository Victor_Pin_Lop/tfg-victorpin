# README #

### Estructura repositorio ###

* Estructura

	En la carpeta principal se encuentran los archivos correspondientes al TFG.

    Dentro de la carpeta "Network" se añadirán las diferentes versiones de las redes junto a las
    funciones necesitadas para hacerlas funcionar.
    En la misma carpeta se añaden los datos con los que se han trabajado pese a que se pueden 
    obtener otros desde la página: http://pems.dot.ca.gov/
	

### Cómo funciona? ###

* Archivos a descargar

    En la carpeta "Networks" se encuentran las Redes utilizadas y utilizadas para la realización 
    de las predicciones.
    
    Es necesario descargar los datos en la carpeta "Networks\Data", puestos que los archivos de 
    "Networks\Data_processed" se pueden obtener ejecutando una red ( se procesan en ella ) aunque
    también se pueden descargar dado que el proceso de procesar los datos toma cierto tiempo según
    la cantidad de información.


* Librerias

	Se hace uso de las siguientes librerias en los diferentes archivos de predicción:
	
	 pandas

     tensorflow

     numpy
	
	 matplotlib.pyplot
     
     datetime

     time

     tqdm

     subprocess



