###
#	Librerias
###
from __future__ import print_function
import os.path
import pandas as pd
import search_data as sd
import Create_data_multiple_files as cdm
import numpy as np
from openpyxl import Workbook

# Multiples archivos

def Create_from_multi_files(Dir_multiple_files, array_sens):
	first_run = True
	
	for sens in array_sens:
		Dir_file = Dir_multiple_files + str(sens) + ".csv"
		if(os.path.isfile(Dir_file)):
			[Intensity_matrix_tmp, Density_matrix_tmp, Speed_matrix_tmp] = sd.distribute_data(Dir_file)
			if(first_run == True):
				Intensity_matrix = np.zeros((1, Intensity_matrix_tmp.shape[0]))
				Density_matrix = np.zeros((1, Density_matrix_tmp.shape[0]))
				Speed_matrix = np.zeros((1, Speed_matrix_tmp.shape[0]))
			Intensity_matrix = np.vstack([Intensity_matrix, Intensity_matrix_tmp])
			Density_matrix = np.vstack([Density_matrix, Density_matrix_tmp])
			Speed_matrix = np.vstack([Speed_matrix, Speed_matrix_tmp])
			first_run = False
			print(" Sensor cargado: ", sens)
	
	Intensity_matrix = np.delete(Intensity_matrix, 0, axis = 0)
	Density_matrix = np.delete(Density_matrix, 0, axis = 0)
	Speed_matrix = np.delete(Speed_matrix, 0, axis = 0)
	
	min_horas_intensity = Intensity_matrix.shape[1]
	min_horas_density = Density_matrix.shape[1]
	min_horas_speed = Speed_matrix.shape[1]
	
	Num_sensors_intensity = Intensity_matrix.shape[0]
	Num_sensors_density = Intensity_matrix.shape[0]
	Num_sensors_speed = Intensity_matrix.shape[0]
	
	Data_intensity = Intensity_matrix
	Data_density = Density_matrix
	Data_speed = Speed_matrix
	
	wb = Workbook(write_only=True)
	ws = wb.create_sheet()
	for irow in range(1):
		ws.append([i for i in range(min_horas_intensity)])
	for irow in range(Num_sensors_intensity):
		ws.append([Data_intensity[irow,i] for i in range(min_horas_intensity)])
	
	wb.save(Dir_multiple_files+'Intensity_BIG_Guill.xlsx') 
	
	wb = Workbook(write_only=True)
	ws = wb.create_sheet()
	for irow in range(1):
		ws.append([i for i in range(min_horas_density)])
	for irow in range(Num_sensors_density):
		ws.append([Data_density[irow,i] for i in range(min_horas_density)])
	
	wb.save(Dir_multiple_files+'Density_BIG_Guill.xlsx') 
	
	wb = Workbook(write_only=True)
	ws = wb.create_sheet()
	for irow in range(1):
		ws.append([i for i in range(min_horas_speed)])
	for irow in range(Num_sensors_speed):
		ws.append([Data_speed[irow,i] for i in range(min_horas_speed)])
	
	wb.save(Dir_multiple_files+'Speed_BIG_Guill.xlsx') 
	
	return Data_intensity, Num_sensors_intensity, min_horas_intensity, Data_speed, Num_sensors_speed, min_horas_speed, Data_density, Num_sensors_density, min_horas_density