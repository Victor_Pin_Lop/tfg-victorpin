###
#	Librerias
###
from __future__ import print_function
import pandas as pd # Animal
import tensorflow as tf # Dunno
import numpy as np # Math
import matplotlib.pyplot as plt # Plot
import matplotlib.ticker as ticker # Axis
import matplotlib.dates as mdates # Date axis
import datetime # Date object
import time # Process bar
from tqdm import tqdm # Process bar
from subprocess import call # Key detector ( stop loop )

def CNN_NET(Data_input_train, Data_input_test, Data_input_val):
	try:
		'''
		# Train
		min_horas_train = int(Data_input_train.shape[1])
		min_sensores = Data_input_train.shape[0]
		
		Data_train_original = Data_input_train[:,0:min_horas_train,:]
		Data_train = Data_input_train[:,0:min_horas_train,:]
		# Normalizar entrada
		Data_train = (Data_train - Data_train.mean(axis = 0))/(Data_train.std(axis = 0))
		
		# Test
		
		min_horas_test = int(Data_input_test.shape[1])
		Data_test = Data_input_test[:,0:min_horas_test,:]
		Data_test_original = Data_test
		Data_test = (Data_test - Data_test.mean(axis = 0))/(Data_test.std(axis = 0))
		
		# Validation
		
		min_horas_val = int(Data_input_val.shape[1])
		Data_val = Data_input_val[:,0:min_horas_val,:]
		Data_val_original = Data_val
		Data_val = (Data_val - Data_val.mean(axis = 0))/(Data_val.std(axis = 0))
		'''
		
		# Train
		min_horas_train = int(Data_input_train.shape[1])
		min_sensores = Data_input_train.shape[0]
		
		Data_train_original = Data_input_train[:,0:min_horas_train,:]
		Data_train = Data_input_train[:,0:min_horas_train,:]
		# Normalizar entrada
		Data_train = (Data_train - Data_train.mean(axis = 0))/(Data_train.std(axis = 0))
		
		# Test
		
		min_horas_test = int(Data_input_test.shape[1])
		Data_test = Data_input_test[:,0:min_horas_test,:]
		Data_test_original = Data_input_test[:,0:min_horas_test,:]
		Data_test = (Data_test - Data_test.mean(axis = 0))/Data_test.std(axis = 0)
		
		
		# Validation
		
		min_horas_val = int(Data_input_val.shape[1])
		Data_val = Data_input_val[:,0:min_horas_val,:]
		Data_val_original = Data_val
		Data_val = (Data_val - Data_val.mean(axis = 0))/Data_val.std(axis = 0)
		
		###
		# Network variables
		###
		
		# Struct data
		num_channels = 3
		num_capas = 1
		batch_size = 72
		pred_hours = 3
		
		# Regularizer
		beta_loss = 0.1
		regularizador = 0
		
		# Var Net
		bn_train = True
		UseXavier = False
		
		# Var optimizer
		start_learning_rate = 1e-4
		stair_steps_learning_rate = 1000
		multiply_learning_rate = 0.1
		max_gradient = 40.
		min_gradient = -40.
		
		# Residual
		Res_W_desv = 0.1
		Res_b_desv = 0.1
		
		# Residual 1
		Dim_filt_R1 = 3
		Num_filters_R1 = 16 #good: 16 # default: 32
		
		# Residual 2
		Dim_filt_R2 = 3
		Num_filters_R2 = 32 #good: 32 # default: 64
		
		# Residual 3
		Dim_filt_R3 = 3
		Num_filters_R3 = 72 #good: 72 # default: 96
		
		# FC (2048)
		FC1_neur = 2048
		FC2_neur = 1024
		FC3_neur = num_capas
		
		FC_W_desv = 0.1
		FC_b_desv = 0.1
		
		# Feed
		num_epochs = 20
		epoch_val = 2 # cada x
		num_steps = int((min_horas_train - batch_size-pred_hours)/batch_size)
		num_steps_val = int((min_horas_val - batch_size-pred_hours)/batch_size)
		num_steps_test = min_horas_test - batch_size-pred_hours
		num_steps_test_b = int((min_horas_test - batch_size-pred_hours)/batch_size)
		graph = tf.Graph()
		
		###
		# CNN
		###
		
		with graph.as_default():
			Train_data_ph = tf.placeholder(tf.float32, shape=(min_sensores, batch_size, num_channels ))
			Original_data_ph = tf.placeholder(tf.float32, shape=(min_sensores, num_capas))
			
			if ( UseXavier == False):
				# Conv
					# # # # # 
				
					# # # 32.1
				R_W_11 = tf.Variable(tf.truncated_normal([Dim_filt_R1, num_channels, Num_filters_R1], stddev = Res_W_desv), dtype = tf.float32)
				R_b_11 = tf.Variable(tf.truncated_normal(shape = [Num_filters_R1], stddev = Res_b_desv), dtype = tf.float32)
				
				R_W_12 = tf.Variable(tf.truncated_normal([Dim_filt_R1, Num_filters_R1,num_channels], stddev = Res_W_desv), dtype = tf.float32)
				R_b_12 = tf.Variable(tf.truncated_normal(shape = [num_channels], stddev = Res_b_desv), dtype = tf.float32)
					# # # 32.1
					# # # 32.2
				R_W_13 = tf.Variable(tf.truncated_normal([Dim_filt_R1, num_channels, Num_filters_R1], stddev = Res_W_desv), dtype = tf.float32)
				R_b_13 = tf.Variable(tf.truncated_normal(shape = [Num_filters_R1], stddev = Res_b_desv), dtype = tf.float32)
				
				R_W_14 = tf.Variable(tf.truncated_normal([Dim_filt_R1, Num_filters_R1,num_channels], stddev = Res_W_desv), dtype = tf.float32)
				R_b_14 = tf.Variable(tf.truncated_normal(shape = [num_channels], stddev = Res_b_desv), dtype = tf.float32)
					# # # 32.2
					# # # 32.3
				R_W_15 = tf.Variable(tf.truncated_normal([Dim_filt_R1, num_channels, Num_filters_R1], stddev = Res_W_desv), dtype = tf.float32)
				R_b_15 = tf.Variable(tf.truncated_normal(shape = [Num_filters_R1], stddev = Res_b_desv), dtype = tf.float32)
				
				R_W_16 = tf.Variable(tf.truncated_normal([Dim_filt_R1, Num_filters_R1,num_channels], stddev = Res_W_desv), dtype = tf.float32)
				R_b_16 = tf.Variable(tf.truncated_normal(shape = [num_channels], stddev = Res_b_desv), dtype = tf.float32)
					# # # 32.3
					# # # 64.1
				R_W_21 = tf.Variable(tf.truncated_normal([Dim_filt_R2, num_channels,Num_filters_R2], stddev = Res_W_desv), dtype = tf.float32)
				R_b_21 = tf.Variable(tf.truncated_normal(shape = [Num_filters_R2], stddev = Res_b_desv), dtype = tf.float32)
				
				R_W_22 = tf.Variable(tf.truncated_normal([Dim_filt_R2, Num_filters_R2,num_channels], stddev = Res_W_desv), dtype = tf.float32)
				R_b_22 = tf.Variable(tf.truncated_normal(shape = [num_channels], stddev = Res_b_desv), dtype = tf.float32)
					# # # 64.2
				R_W_23 = tf.Variable(tf.truncated_normal([Dim_filt_R2, num_channels,Num_filters_R2], stddev = Res_W_desv), dtype = tf.float32)
				R_b_23 = tf.Variable(tf.truncated_normal(shape = [Num_filters_R2], stddev = Res_b_desv), dtype = tf.float32)
				
				R_W_24 = tf.Variable(tf.truncated_normal([Dim_filt_R2, Num_filters_R2,num_channels], stddev = Res_W_desv), dtype = tf.float32)
				R_b_24 = tf.Variable(tf.truncated_normal(shape = [num_channels], stddev = Res_b_desv), dtype = tf.float32)
					# # # 64.2
					# # # 64.3
				R_W_25 = tf.Variable(tf.truncated_normal([Dim_filt_R2, num_channels,Num_filters_R2], stddev = Res_W_desv), dtype = tf.float32)
				R_b_25 = tf.Variable(tf.truncated_normal(shape = [Num_filters_R2], stddev = Res_b_desv), dtype = tf.float32)
				
				R_W_26 = tf.Variable(tf.truncated_normal([Dim_filt_R2, Num_filters_R2,num_channels], stddev = Res_W_desv), dtype = tf.float32)
				R_b_26 = tf.Variable(tf.truncated_normal(shape = [num_channels], stddev = Res_b_desv), dtype = tf.float32)
					# # # 64.3
					# # # 96.1
				R_W_31 = tf.Variable(tf.truncated_normal([Dim_filt_R3, num_channels,Num_filters_R3], stddev = Res_W_desv), dtype = tf.float32)
				R_b_31 = tf.Variable(tf.truncated_normal(shape = [Num_filters_R3], stddev = Res_b_desv), dtype = tf.float32)
				
				R_W_32 = tf.Variable(tf.truncated_normal([Dim_filt_R3, Num_filters_R3,num_channels], stddev = Res_W_desv), dtype = tf.float32)
				R_b_32 = tf.Variable(tf.truncated_normal(shape = [num_channels], stddev = Res_b_desv), dtype = tf.float32)
					# # # 96.1
					# # # 96.2
				R_W_33 = tf.Variable(tf.truncated_normal([Dim_filt_R3, num_channels,Num_filters_R3], stddev = Res_W_desv), dtype = tf.float32)
				R_b_33 = tf.Variable(tf.truncated_normal(shape = [Num_filters_R3], stddev = Res_b_desv), dtype = tf.float32)
				
				R_W_34 = tf.Variable(tf.truncated_normal([Dim_filt_R3, Num_filters_R3,num_channels], stddev = Res_W_desv), dtype = tf.float32)
				R_b_34 = tf.Variable(tf.truncated_normal(shape = [num_channels], stddev = Res_b_desv), dtype = tf.float32)
					# # # 96.2
					# # # 96.3
				R_W_35 = tf.Variable(tf.truncated_normal([Dim_filt_R3, num_channels,Num_filters_R3], stddev = Res_W_desv), dtype = tf.float32)
				R_b_35 = tf.Variable(tf.truncated_normal(shape = [Num_filters_R3], stddev = Res_b_desv), dtype = tf.float32)
				
				R_W_36 = tf.Variable(tf.truncated_normal([Dim_filt_R3, Num_filters_R3,num_channels], stddev = Res_W_desv), dtype = tf.float32)
				R_b_36 = tf.Variable(tf.truncated_normal(shape = [num_channels], stddev = Res_b_desv), dtype = tf.float32)
					# # # 96.3
			else:
				# Conv
					# # # # # 
				
					# # # 32.1
				R_W_11 = tf.get_variable( 'R_W_11',shape=[Dim_filt_R1, num_channels, Num_filters_R1],initializer=tf.contrib.layers.xavier_initializer())
				R_b_11 = tf.get_variable( 'R_b_11',shape=[Num_filters_R1],initializer=tf.contrib.layers.xavier_initializer())
				
				R_W_12 = tf.get_variable( 'R_W_12',shape=[Dim_filt_R1, Num_filters_R1, num_channels],initializer=tf.contrib.layers.xavier_initializer())
				R_b_12 = tf.get_variable( 'R_b_12',shape=[num_channels],initializer=tf.contrib.layers.xavier_initializer())
					# # # 32.1
					# # # 32.2
				R_W_13 = tf.get_variable( 'R_W_13',shape=[Dim_filt_R1, num_channels, Num_filters_R1],initializer=tf.contrib.layers.xavier_initializer())
				R_b_13 = tf.get_variable( 'R_b_13',shape=[Num_filters_R1],initializer=tf.contrib.layers.xavier_initializer())
				
				R_W_14 = tf.get_variable( 'R_W_14',shape=[Dim_filt_R1, Num_filters_R1, num_channels],initializer=tf.contrib.layers.xavier_initializer())
				R_b_14 = tf.get_variable( 'R_b_14',shape=[num_channels],initializer=tf.contrib.layers.xavier_initializer())
					# # # 32.2
					# # # 32.3
				R_W_15 = tf.get_variable( 'R_W_15',shape=[Dim_filt_R1, num_channels, Num_filters_R1],initializer=tf.contrib.layers.xavier_initializer())
				R_b_15 = tf.get_variable( 'R_b_15',shape=[Num_filters_R1],initializer=tf.contrib.layers.xavier_initializer())
				
				R_W_16 = tf.get_variable( 'R_W_16',shape=[Dim_filt_R1, Num_filters_R1, num_channels],initializer=tf.contrib.layers.xavier_initializer())
				R_b_16 = tf.get_variable( 'R_b_16',shape=[num_channels],initializer=tf.contrib.layers.xavier_initializer())
					# # # 32.3
					# # # 64.1
				R_W_21 = tf.get_variable( 'R_W_21',shape=[Dim_filt_R2, num_channels, Num_filters_R2],initializer=tf.contrib.layers.xavier_initializer())
				R_b_21 = tf.get_variable( 'R_b_21',shape=[Num_filters_R2],initializer=tf.contrib.layers.xavier_initializer())
				
				R_W_22 = tf.get_variable( 'R_W_22',shape=[Dim_filt_R2, Num_filters_R2, num_channels],initializer=tf.contrib.layers.xavier_initializer())
				R_b_22 = tf.get_variable( 'R_b_22',shape=[num_channels],initializer=tf.contrib.layers.xavier_initializer())
					# # # 64.2
				R_W_23 = tf.get_variable( 'R_W_23',shape=[Dim_filt_R2, num_channels, Num_filters_R2],initializer=tf.contrib.layers.xavier_initializer())
				R_b_23 = tf.get_variable( 'R_b_23',shape=[Num_filters_R2],initializer=tf.contrib.layers.xavier_initializer())
				
				R_W_24 = tf.get_variable( 'R_W_24',shape=[Dim_filt_R2, Num_filters_R2, num_channels],initializer=tf.contrib.layers.xavier_initializer())
				R_b_24 = tf.get_variable( 'R_b_24',shape=[num_channels],initializer=tf.contrib.layers.xavier_initializer())
					# # # 64.2
					# # # 64.3
				R_W_25 = tf.get_variable( 'R_W_25',shape=[Dim_filt_R2, num_channels, Num_filters_R2],initializer=tf.contrib.layers.xavier_initializer())
				R_b_25 = tf.get_variable( 'R_b_25',shape=[Num_filters_R2],initializer=tf.contrib.layers.xavier_initializer())
				
				R_W_26 = tf.get_variable( 'R_W_26',shape=[Dim_filt_R2, Num_filters_R2, num_channels],initializer=tf.contrib.layers.xavier_initializer())
				R_b_26 = tf.get_variable( 'R_b_26',shape=[num_channels],initializer=tf.contrib.layers.xavier_initializer())
					# # # 64.3
					# # # 96.1
				R_W_31 = tf.get_variable( 'R_W_31',shape=[Dim_filt_R3, num_channels, Num_filters_R3],initializer=tf.contrib.layers.xavier_initializer())
				R_b_31 = tf.get_variable( 'R_b_31',shape=[Num_filters_R3],initializer=tf.contrib.layers.xavier_initializer())
				
				R_W_32 = tf.get_variable( 'R_W_32',shape=[Dim_filt_R3, Num_filters_R3, num_channels],initializer=tf.contrib.layers.xavier_initializer())
				R_b_32 = tf.get_variable( 'R_b_32',shape=[num_channels],initializer=tf.contrib.layers.xavier_initializer())
					# # # 96.1
					# # # 96.2
				R_W_33 = tf.get_variable( 'R_W_33',shape=[Dim_filt_R3, num_channels, Num_filters_R3],initializer=tf.contrib.layers.xavier_initializer())
				R_b_33 = tf.get_variable( 'R_b_33',shape=[Num_filters_R3],initializer=tf.contrib.layers.xavier_initializer())
				
				R_W_34 = tf.get_variable( 'R_W_34',shape=[Dim_filt_R3, Num_filters_R3, num_channels],initializer=tf.contrib.layers.xavier_initializer())
				R_b_34 = tf.get_variable( 'R_b_34',shape=[num_channels],initializer=tf.contrib.layers.xavier_initializer())
					# # # 96.2
					# # # 96.3
				R_W_35 = tf.get_variable( 'R_W_35',shape=[Dim_filt_R3, num_channels, Num_filters_R3],initializer=tf.contrib.layers.xavier_initializer())
				R_b_35 = tf.get_variable( 'R_b_35',shape=[Num_filters_R3],initializer=tf.contrib.layers.xavier_initializer())
				
				R_W_36 = tf.get_variable( 'R_W_36',shape=[Dim_filt_R3, Num_filters_R3, num_channels],initializer=tf.contrib.layers.xavier_initializer())
				R_b_36 = tf.get_variable( 'R_b_36',shape=[num_channels],initializer=tf.contrib.layers.xavier_initializer())
					# # # 96.3
				
				# # # # # 32.1
			conv11 = tf.nn.conv1d(Train_data_ph,R_W_11, 1,padding='SAME') + R_b_11
			conv11 = tf.contrib.layers.batch_norm(conv11, is_training = bn_train, updates_collections = None)
			conv11 = tf.nn.relu(conv11)
			
			conv12 = tf.nn.conv1d(conv11,R_W_12, 1,padding='SAME') + R_b_12
			conv12 = tf.contrib.layers.batch_norm(conv12, is_training = bn_train, updates_collections = None)
			conv12 = tf.nn.relu(conv12)
				# # # # # 32.1
			conv12 = tf.identity(conv12 + Train_data_ph)
				# # # # # 32.2
			conv13 = tf.nn.conv1d(conv12,R_W_13, 1,padding='SAME') + R_b_13
			conv13 = tf.contrib.layers.batch_norm(conv13, is_training = bn_train, updates_collections = None)
			conv13 = tf.nn.relu(conv13)
			
			conv14 = tf.nn.conv1d(conv13,R_W_14, 1,padding='SAME') + R_b_14
			conv14 = tf.contrib.layers.batch_norm(conv14, is_training = bn_train, updates_collections = None)
			conv14 = tf.nn.relu(conv14)
				# # # # # 32.2
			conv14 = tf.identity(conv14 + conv12)
				# # # # # 32.3
			conv15 = tf.nn.conv1d(conv14,R_W_15, 1,padding='SAME') + R_b_15
			conv15 = tf.contrib.layers.batch_norm(conv15, is_training = bn_train, updates_collections = None)
			conv15 = tf.nn.relu(conv15)
			
			conv16 = tf.nn.conv1d(conv15,R_W_16, 1,padding='SAME') + R_b_16
			conv16 = tf.contrib.layers.batch_norm(conv16, is_training = bn_train, updates_collections = None)
			conv16 = tf.nn.relu(conv16)
				# # # # # 32.3
			conv16 = tf.identity(conv16 + conv14)
				# # # # # 64.1
			conv21 = tf.nn.conv1d(conv16,R_W_21, 1,padding='SAME') + R_b_21
			conv21 = tf.contrib.layers.batch_norm(conv21, is_training = bn_train, updates_collections = None)
			conv21 = tf.nn.relu(conv21)
			
			conv22 = tf.nn.conv1d(conv21,R_W_22, 1,padding='SAME') + R_b_22
			conv22 = tf.contrib.layers.batch_norm(conv22, is_training = bn_train, updates_collections = None)
			conv22 = tf.nn.relu(conv22)
				# # # # # 64.1
			conv22 = tf.identity(conv22 + conv16)
				# # # # # 64.2
			conv23 = tf.nn.conv1d(conv22,R_W_23, 1,padding='SAME') + R_b_23
			conv23 = tf.contrib.layers.batch_norm(conv23, is_training = bn_train, updates_collections = None)
			conv23 = tf.nn.relu(conv23)
			
			conv24 = tf.nn.conv1d(conv23,R_W_24, 1,padding='SAME') + R_b_24
			conv24 = tf.contrib.layers.batch_norm(conv24, is_training = bn_train, updates_collections = None)
			conv24 = tf.nn.relu(conv24)
				# # # # # 64.2
			conv24 = tf.identity(conv24 + conv22)
				# # # # # 64.3
			conv25 = tf.nn.conv1d(conv24,R_W_25, 1,padding='SAME') + R_b_25
			conv25 = tf.contrib.layers.batch_norm(conv25, is_training = bn_train, updates_collections = None)
			conv25 = tf.nn.relu(conv25)
			
			conv26 = tf.nn.conv1d(conv25,R_W_26, 1,padding='SAME') + R_b_26
			conv26 = tf.contrib.layers.batch_norm(conv26, is_training = bn_train, updates_collections = None)
			conv26 = tf.nn.relu(conv26)
				# # # # # 64.3
			conv26 = tf.identity(conv26 + conv24)
				# # # # # 96.1
			conv31 = tf.nn.conv1d(conv26,R_W_31, 1,padding='SAME') + R_b_31
			conv31 = tf.contrib.layers.batch_norm(conv31, is_training = bn_train, updates_collections = None)
			conv31 = tf.nn.relu(conv31)
			
			conv32 = tf.nn.conv1d(conv31,R_W_32, 1,padding='SAME') + R_b_32
			conv32 = tf.contrib.layers.batch_norm(conv32, is_training = bn_train, updates_collections = None)
			conv32 = tf.nn.relu(conv32)
				# # # # # 96.1
			conv32 = tf.identity(conv32 + conv26)
				# # # # # 96.2
			conv33 = tf.nn.conv1d(conv32,R_W_33, 1,padding='SAME') + R_b_33
			conv33 = tf.contrib.layers.batch_norm(conv33, is_training = bn_train, updates_collections = None)
			conv33 = tf.nn.relu(conv33)
			
			conv34 = tf.nn.conv1d(conv33,R_W_34, 1,padding='SAME') + R_b_34
			conv34 = tf.contrib.layers.batch_norm(conv34, is_training = bn_train, updates_collections = None)
			conv34 = tf.nn.relu(conv34)
				# # # # # 96.2
			conv34 = tf.identity(conv34 + conv32)
				# # # # # 96.3
			conv35 = tf.nn.conv1d(conv34,R_W_35, 1,padding='SAME') + R_b_35
			conv35 = tf.contrib.layers.batch_norm(conv35, is_training = bn_train, updates_collections = None)
			conv35 = tf.nn.relu(conv35)
			
			conv36 = tf.nn.conv1d(conv35,R_W_36, 1,padding='SAME') + R_b_36
			conv36 = tf.contrib.layers.batch_norm(conv36, is_training = bn_train, updates_collections = None)
			conv36 = tf.nn.relu(conv36)
				# # # # # 96.3
			conv36 = tf.identity(conv36 + conv34)
			
			conv36_shape = conv36.get_shape().as_list()
			conv36 = tf.reshape(conv36,[conv36_shape[0], conv36_shape[1]*conv36_shape[2]]) 
			
			# FC
			if( UseXavier == True):
				FC1_W = tf.get_variable( 'FC1_W',shape=[batch_size*num_channels, FC1_neur],initializer=tf.contrib.layers.xavier_initializer())
				FC1_b = tf.get_variable( 'FC1_b',shape=[FC1_neur],initializer=tf.contrib.layers.xavier_initializer())
				
				FC2_W = tf.get_variable( 'FC2_W',shape=[FC1_neur, FC2_neur],initializer=tf.contrib.layers.xavier_initializer())
				FC2_b = tf.get_variable( 'FC2_b',shape=[FC2_neur],initializer=tf.contrib.layers.xavier_initializer())
				
				FC3_W = tf.get_variable( 'FC3_W',shape=[FC2_neur, FC3_neur],initializer=tf.contrib.layers.xavier_initializer())
				FC3_b = tf.get_variable( 'FC3_b',shape=[FC3_neur],initializer=tf.contrib.layers.xavier_initializer())
				
			else:
				FC1_W = tf.Variable(tf.truncated_normal([batch_size*num_channels, FC1_neur], stddev = FC_W_desv), dtype = tf.float32)
				FC1_b = tf.Variable(tf.truncated_normal(shape = [FC1_neur], stddev = FC_b_desv), dtype = tf.float32)
				
				FC2_W = tf.Variable(tf.truncated_normal([FC1_neur, FC2_neur], stddev = FC_W_desv), dtype = tf.float32)
				FC2_b = tf.Variable(tf.truncated_normal(shape = [FC2_neur], stddev = FC_b_desv), dtype = tf.float32)
				
				FC3_W = tf.Variable(tf.truncated_normal([FC2_neur, FC3_neur], stddev = FC_W_desv), dtype = tf.float32)
				FC3_b = tf.Variable(tf.truncated_normal(shape = [FC3_neur], stddev = FC_b_desv), dtype = tf.float32)
			
			FC1 = tf.matmul(conv36,FC1_W) + FC1_b
			FC1 = tf.contrib.layers.batch_norm(FC1, is_training = bn_train, updates_collections = None)
			FC1 = tf.nn.relu(FC1)
			
			FC2 = tf.matmul(FC1,FC2_W) + FC2_b
			
			FC3 = tf.matmul(FC2,FC3_W) + FC3_b
			
			prediction = FC3
			
			# MSE
			#loss = tf.nn.l2_loss(prediction-Original_data_ph)
			loss = (0.5)*(tf.norm(prediction-Original_data_ph))**2
			MAE = tf.reduce_mean(tf.abs(prediction - Original_data_ph))
			MAPE = tf.reduce_mean(tf.abs((prediction - Original_data_ph)/Original_data_ph))
			
			regularizador = 0
			regularizador = regularizador + tf.nn.l2_loss(R_W_11) + tf.nn.l2_loss(R_b_11)
			regularizador = regularizador + tf.nn.l2_loss(R_W_12) + tf.nn.l2_loss(R_b_12)
			regularizador = regularizador + tf.nn.l2_loss(R_W_13) + tf.nn.l2_loss(R_b_13)
			regularizador = regularizador + tf.nn.l2_loss(R_W_14) + tf.nn.l2_loss(R_b_14)
			regularizador = regularizador + tf.nn.l2_loss(R_W_15) + tf.nn.l2_loss(R_b_15)
			regularizador = regularizador + tf.nn.l2_loss(R_W_16) + tf.nn.l2_loss(R_b_16)
			regularizador = regularizador + tf.nn.l2_loss(R_W_21) + tf.nn.l2_loss(R_b_21)
			regularizador = regularizador + tf.nn.l2_loss(R_W_22) + tf.nn.l2_loss(R_b_22)
			regularizador = regularizador + tf.nn.l2_loss(R_W_23) + tf.nn.l2_loss(R_b_23)
			regularizador = regularizador + tf.nn.l2_loss(R_W_24) + tf.nn.l2_loss(R_b_24)
			regularizador = regularizador + tf.nn.l2_loss(R_W_25) + tf.nn.l2_loss(R_b_25)
			regularizador = regularizador + tf.nn.l2_loss(R_W_26) + tf.nn.l2_loss(R_b_26)
			regularizador = regularizador + tf.nn.l2_loss(R_W_31) + tf.nn.l2_loss(R_b_31)
			regularizador = regularizador + tf.nn.l2_loss(R_W_32) + tf.nn.l2_loss(R_b_32)
			regularizador = regularizador + tf.nn.l2_loss(R_W_33) + tf.nn.l2_loss(R_b_33)
			regularizador = regularizador + tf.nn.l2_loss(R_W_34) + tf.nn.l2_loss(R_b_34)
			regularizador = regularizador + tf.nn.l2_loss(R_W_35) + tf.nn.l2_loss(R_b_35)
			regularizador = regularizador + tf.nn.l2_loss(R_W_36) + tf.nn.l2_loss(R_b_36)
			regularizador = regularizador + tf.nn.l2_loss(FC1_W) + tf.nn.l2_loss(FC1_b)
			regularizador = regularizador + tf.nn.l2_loss(FC2_W) + tf.nn.l2_loss(FC2_b)
			regularizador = regularizador + tf.nn.l2_loss(FC3_W) + tf.nn.l2_loss(FC3_b)
			
			loss_b = loss + beta_loss*regularizador
			
			# Optimizer GradientDescent + Exponential + Gradiente capado ( v1.1 )
			global_step_lr = tf.Variable(0, dtype = tf.float32)
			learning_rate = tf.train.exponential_decay(start_learning_rate, global_step_lr, stair_steps_learning_rate, multiply_learning_rate, staircase=True)
			optimizer = tf.train.GradientDescentOptimizer(learning_rate)
			gradients = optimizer.compute_gradients(loss_b)
			def ClipIfNotNone(grad):
				if grad is None:
					return grad
				return tf.clip_by_value(grad, min_gradient, max_gradient)
			
			clipped_gradients = [(ClipIfNotNone(grad), var) for grad, var in gradients]
			train = optimizer.apply_gradients(clipped_gradients, global_step=global_step_lr)
			
			# Precision
			correct_prediction = tf.equal( tf.round( prediction ), tf.round( Original_data_ph ) )
			accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
			
			
		
		###
		# Feeder
		###
		
		with tf.Session(graph=graph) as sess:
			tf.global_variables_initializer().run()
			
			# Plot 
			Sensor_to_plot = 5
			Var_to_plot = 1 # [0,1,2] "Intensity, Speed, Density"
			
			#########
			# TRAIN & VALIDATION
			#########
			
			Total_loss_train = np.zeros((num_epochs))
			Total_loss_val = np.zeros((int((num_epochs)/epoch_val)))
			val_epoch = 0
			
			original_data = np.zeros((min_sensores, num_capas))
			# TRAIN var
			Epoch_loss_train = np.zeros((num_steps))
			Epoch_mae_train = np.zeros((num_steps))
			Epoch_mape_train = np.zeros((num_steps))
			Epoch_accuracy_train = np.zeros((num_steps))
			
			# VALIDATION var
			Epoch_loss_val = np.zeros((num_steps_val))
			Epoch_mae_val = np.zeros((num_steps_val))
			Epoch_mape_val = np.zeros((num_steps_val))
			Epoch_accuracy_val = np.zeros((num_steps_val))
			
			print(' \n \n-- -- Running CNN -- -- \n \n ')
			for epoch in range(num_epochs):
				
				print(' -- Epoch ', epoch)
				
				print(' - TRAIN - ')
				#for step in tqdm(range(num_steps)):
				for step in tqdm(np.random.permutation(num_steps)):
					try:
						offset = (step*batch_size) % (min_horas_train - batch_size)
						
						batch_data = Data_train[:,offset:offset+batch_size,:]
						original_data[:,0] = Data_train_original[:,offset+batch_size+pred_hours,Var_to_plot]
						
						feed_dict = {Train_data_ph : batch_data, Original_data_ph : original_data}
						_,loss_train, MAE_train, MAPE_train, acc_train = sess.run([train, loss, MAE, MAPE, accuracy], feed_dict = feed_dict)
						
						Epoch_loss_train[step] = np.mean(loss_train)
						Epoch_mae_train[step] = MAE_train
						Epoch_mape_train[step] = MAPE_train*100
						Epoch_accuracy_train[step] = acc_train*100
						
						time.sleep(0.0001)
					except KeyboardInterrupt:
						break
				Total_loss_train[epoch] = np.mean(Epoch_loss_train)
				
				print(' - MAE train ', np.mean(Epoch_mae_train))
				
				if((epoch-1)%epoch_val==0)and(epoch!=0):
					
					print(' - VALIDATION - ')
					#for step in tqdm(range(num_steps_val)):
					for step in tqdm(np.random.permutation(num_steps_val)):
						try:
							offset = (step*batch_size) % (min_horas_val - batch_size)
							
							batch_data = Data_val[:,offset:offset+batch_size,:]
							original_data[:,0] = Data_val_original[:,offset+batch_size+pred_hours,Var_to_plot]
							
							feed_dict = {Train_data_ph : batch_data, Original_data_ph : original_data}
							loss_val, MAE_val, MAPE_val, acc_val = sess.run([loss, MAE, MAPE, accuracy], feed_dict = feed_dict)
							
							Epoch_loss_val[step] = np.mean(loss_val)
							Epoch_mae_val[step] = MAE_val
							Epoch_mape_val[step] = MAPE_val*100
							Epoch_accuracy_val[step] = acc_val*100
							
							time.sleep(0.0001)
						except KeyboardInterrupt:
							break
					Total_loss_val[val_epoch] = np.mean(Epoch_loss_val)
					val_epoch += 1
					print(' - MAE val ', np.mean(Epoch_mae_val))
				
			
			print(" --- TRAIN ---")
			print(" - Last epoch MAE ", np.mean(Epoch_mae_train))
			print(" - Last epoch MAPE ", np.mean(Epoch_mape_train))
			print(' - Last epoch accuracy ' , np.mean(Epoch_accuracy_train))
			print(' - Last epoch loss ' , np.mean(Epoch_loss_train))
			print(' - Total train loss ' , np.mean(Total_loss_train))
			
			print(" --- VALIDATION ---")
			print(" - Last epoch MAE ", np.mean(Epoch_mae_val))
			print(" - Last epoch MAPE ", np.mean(Epoch_mape_val))
			print(' - Last epoch accuracy ' , np.mean(Epoch_accuracy_val))
			print(' - Last epoch loss ' , np.mean(Epoch_loss_val))
			print(' - Total validation loss ' , np.mean(Total_loss_val))
			
			#########
			# To PLOT Loss
			#########
			
			if True:
				
				plt.figure()
				Plot_total_loss = './plots/CNN_Total_loss.png'
				
				train = Total_loss_train
				val = Total_loss_val
				n = int(len(train)/len(val))
				
				plt.plot(train, label = 'Training loss')
				plt.plot([x + n - 1 for x in range(0, len(train), n)], val, '--', label='Validation loss')
				plt.legend(fontsize=6)
				plt.ylabel('Loss')
				plt.xlabel('Epocas')
				#plt.show()
				plt.savefig(Plot_total_loss, dpi = 800)
			
			#########
			# To TXT
			#########
			
			MAE_train_txt = np.reshape(np.mean(Epoch_mae_train),(1,1))
			MAPE_train_txt = np.reshape(np.mean(Epoch_mape_train),(1,1))
			Total_loss_train_txt = np.reshape(np.mean(Total_loss_train),(1,1))
			Total_acc_train_txt = np.reshape(np.mean(Epoch_accuracy_train),(1,1))
			
			np.savetxt("./plots/CNN_MAE_train.txt",MAE_train_txt, delimiter='\n')
			np.savetxt("./plots/CNN_MAPE_train.txt",MAPE_train_txt, delimiter='\n')
			np.savetxt("./plots/CNN_Last_Total_Loss_train.txt",Total_loss_train_txt, delimiter='\n')
			np.savetxt("./plots/CNN_Last_Total_acc_train.txt",Total_acc_train_txt, delimiter='\n')
			
			MAE_val_txt = np.reshape(np.mean(Epoch_mae_val),(1,1))
			MAPE_val_txt = np.reshape(np.mean(Epoch_mape_val),(1,1))
			Total_loss_val_txt = np.reshape(np.mean(Total_loss_val),(1,1))
			Total_acc_val_txt = np.reshape(np.mean(Epoch_accuracy_val),(1,1))
			
			np.savetxt("./plots/CNN_MAE_val.txt",MAE_val_txt, delimiter='\n')
			np.savetxt("./plots/CNN_MAPE_val.txt",MAPE_val_txt, delimiter='\n')
			np.savetxt("./plots/CNN_Last_Total_loss_val.txt",Total_loss_val_txt, delimiter='\n')
			np.savetxt("./plots/CNN_Last_Total_acc_val.txt",Total_acc_val_txt, delimiter='\n')
			
			#########
			# TEST 
			#########
			
			if True:
				# Plot 
				Sensor_to_plot = 5
				Var_to_plot = 1 # [0,1,2] "Intensity, Speed, Density"
				
				original_data = np.zeros((min_sensores,1))
				Epoch_pred_test = np.zeros((min_sensores, num_steps_test))
				
				Epoch_loss_test = np.zeros((num_steps_test_b))
				Epoch_mae_test = np.zeros((num_steps_test_b))
				Epoch_mape_test = np.zeros((num_steps_test_b))
				Epoch_accuracy_test = np.zeros((num_steps_test_b))
				
				print(' - TESTING - ')
				
				for step in tqdm(range(num_steps_test)):
				#for step in tqdm(np.random.permutation(num_steps_val)):
					try:
						#offset = (step*batch_size) % (min_horas_test - batch_size)
						offset = step+batch_size
						
						batch_data = Data_test[:,step:offset,:]
						original_data[:,0] = Data_test_original[:,offset+pred_hours,Var_to_plot]
						
						feed_dict = {Train_data_ph : batch_data, Original_data_ph : original_data}
						loss_test, MAE_test, MAPE_test, acc_test, pred_test = sess.run([loss, MAE, MAPE, accuracy, prediction], feed_dict = feed_dict)
						
						Epoch_pred_test[:,step] = pred_test[:,0]
						
						time.sleep(0.0001)
					except KeyboardInterrupt:
						break
				
				#for step in tqdm(range(num_steps_test_b)):
				for step in tqdm(np.random.permutation(num_steps_test_b)):
					try:
						offset = (step*batch_size) % (min_horas_test - batch_size)
						#offset = step+batch_size
						
						batch_data = Data_test[:,offset:offset+batch_size,:]
						original_data[:,0] = Data_test_original[:,offset+batch_size+pred_hours,Var_to_plot]
						
						feed_dict = {Train_data_ph : batch_data, Original_data_ph : original_data}
						loss_test, MAE_test, MAPE_test, acc_test = sess.run([loss, MAE, MAPE, accuracy], feed_dict = feed_dict)
						
						Epoch_loss_test[step] = np.mean(loss_test)
						Epoch_mae_test[step] = MAE_test
						Epoch_mape_test[step] = MAPE_test*100
						Epoch_accuracy_test[step] = acc_test*100
						
						time.sleep(0.0001)
					except KeyboardInterrupt:
						break
				
				print(" --- Testing ---")
				print(" - Epoch MAE ", np.mean(Epoch_mae_test))
				print(" - Epoch MAPE ", np.mean(Epoch_mape_test))
				print(' - Epoch accuracy ' , np.mean(Epoch_accuracy_test))
				print(' - Epoch loss ' , np.mean(Epoch_loss_test))
				
				#########
				# To PLOT Prediction
				#########
				
				if True:
					datemin = datetime.datetime(2013, 1, 1, 0, 0)
					datemax = datetime.datetime(2013, 12, 31, 23, 0)
					plot_save_PRED = './plots/CNN_Test_PRED.png'
					plt.figure()
					y = Data_test_original[Sensor_to_plot,:,Var_to_plot]
					x = [datemin + datetime.timedelta(minutes=i*5) for i in range(len(y))]
					y_p = Epoch_pred_test[Sensor_to_plot, :]
					y = y[76275+batch_size+pred_hours:77727+batch_size+pred_hours]
					x = x[76275+batch_size+pred_hours:77727+batch_size+pred_hours]
					y_p = y_p[76275:77727]
					plt.xticks(x, rotation='vertical',fontsize=6)
					plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%A - %H:%M'))
					plt.gca().xaxis.set_major_locator(mdates.HourLocator(interval = 2))
					plt.gcf().autofmt_xdate()
					plt.ylabel('Velocidad en el sensor [km/h]',fontsize=12)
					plt.xlabel('Dia - Hora',fontsize=12)
					plt.plot(x,y, label='Velocidad original')
					plt.plot(x,y_p, label = 'Velocidad predicha')
					plt.legend(fontsize=6)
					plt.tight_layout()
				#	plt.show()
					plt.savefig(plot_save_PRED,dpi=800)
				
				#########
				# To PLOT Loss
				#########
				
				if False:
					
					plt.figure()
					Plot_total_loss = './plots/CNN_Epoch_loss.png'
					
					train = Epoch_loss_train
					val = Epoch_loss_val
					test = Epoch_loss_test
					
					n = int(len(train)/len(val))
					m = int(len(train)/len(test))
					
					plt.plot(train)
					plt.plot([x + n - 1 for x in range(0, len(train), n)], val, '--')
					plt.plot([x + m - 1 for x in range(0, len(train), m)], test, '-.', color = 'red')
					#plt.show()
					plt.savefig(Plot_total_loss, dpi = 800)
				
				#########
				# To TXT
				#########
				
				if True:
					MAE_test_txt = np.reshape(np.mean(Epoch_mae_test),(1,1))
					MAPE_test_txt = np.reshape(np.mean(Epoch_mape_test),(1,1))
					Total_acc_test_txt = np.reshape(np.mean(Epoch_accuracy_test),(1,1))
					Total_loss_test_txt = np.reshape(np.mean(Epoch_loss_test),(1,1))
					
					np.savetxt("./plots/CNN_MAE_test.txt",MAE_test_txt, delimiter='\n')
					np.savetxt("./plots/CNN_MAPE_test.txt",MAPE_test_txt, delimiter='\n')
					np.savetxt("./plots/CNN_Last_Total_acc_test.txt",Total_acc_test_txt, delimiter='\n')
					np.savetxt("./plots/CNN_Last_Total_Loss_test.txt",Total_loss_test_txt, delimiter='\n')
			
	except ValueError as Error:
		print(" Error : ", Error)
	return





