

###
# Librerias
###

from __future__ import print_function
import Read_data as rd
import RNN
import RNN_1
import MLP
import MLP_1
import CNN
import CNN_1
import TestNet

###
# Variables globales
### 

Data_loaded_train = []
Data_loaded_test = []
Data_loaded_val = []

###
# Menu principal
###

while(True):
	# Variables menu
	Opcion_elegida = ""
	
	# Menu principal
	print("\n")
	print(" -- -- Menu principal -- -- ")
	print("\n")
	print(" 1 - Leer datos de diferentes archivos. ")
	print("\n")
	print(" 2 - Entrenar RNN ( Recurrent Neural Network ) ")
	print("\n")
	print(" 3 - Entrenar MLP ( Multi Layer Percepton ) ( todos sensores ) ")
	print("\n")
	print(" 4 - Entrenar CNN ( Convolutional Neural Network ) ")
	print("\n")
	print(" 5 - Entrenar RNN ( Recurrent Neural Network ) ( 1 sensor ) ")
	print("\n")
	print(" 6 - Entrenar MLP ( Multi Layer Percepton ) ( 1 sensor ) ")
	print("\n")
	print(" 7 - Entrenar CNN ( Convolutional Neural Network ) ( 1 sensor ) ")
	print("\n")
	print(" 8 - Combinaciones automaticas. ")
	print("\n")
	print(" 9 - Combinaciones RNN. ")
	print("\n")
	print(" 10 - Test Net. ")
	print("\n")
	print("\n")
	
	# Acciones a realizar
	while(True):
		Opcion_elegida = raw_input('  Escoja una opcion ')
		if  ( Opcion_elegida == "1" ) or ( Opcion_elegida == "2" ) or ( Opcion_elegida == "3" ) or ( Opcion_elegida == "4" ) or ( Opcion_elegida == "5" ) or ( Opcion_elegida == "6" )or ( Opcion_elegida == "7" )or ( Opcion_elegida == "8" )or ( Opcion_elegida == "9" )or ( Opcion_elegida == "10" ):
			break 
	
	
	if( Opcion_elegida == "1"):
		reload(rd)
		Data_loaded_train, Data_loaded_test, Data_loaded_val = rd.Read_data(Data_loaded_train, Data_loaded_test, Data_loaded_val)
		
	
	if( Opcion_elegida == "2"):
		if (len(Data_loaded_train) != 0) and(len(Data_loaded_test) != 0)and(len( Data_loaded_val) != 0):
			reload(RNN)
			RNN.RNN_NET(Data_loaded_train, Data_loaded_test, Data_loaded_val)
			print("\n \n")
			raw_input(" RNN finalizado. Pulse ENTER. ")
			print("\n \n")
		else:
			print("\n")
			print(" No se han cargado los datos. ")
			raw_input("")
	
	if( Opcion_elegida == "3"):
		if (len(Data_loaded_train) != 0) and(len(Data_loaded_test) != 0)and(len( Data_loaded_val) != 0):
			reload(MLP)
			MLP.MLP_NET(Data_loaded_train, Data_loaded_test, Data_loaded_val)
			print("\n \n")
			raw_input(" MLP finalizado. Pulse ENTER. ")
			print("\n \n")
		else:
			print("\n")
			print(" No se han cargado los datos. ")
			raw_input("")
	
	if( Opcion_elegida == "4"):
		if (len(Data_loaded_train) != 0) and(len(Data_loaded_test) != 0)and(len( Data_loaded_val) != 0):
			reload(CNN)
			CNN.CNN_NET(Data_loaded_train, Data_loaded_test, Data_loaded_val)
			print("\n \n")
			raw_input(" CNN finalizado. Pulse ENTER. ")
			print("\n \n")
		else:
			print("\n")
			print(" No se han cargado los datos. ")
			raw_input("")
	if( Opcion_elegida == "5"):
		if (len(Data_loaded_train) != 0) and(len(Data_loaded_test) != 0)and(len( Data_loaded_val) != 0):
			reload(RNN_1)
			RNN_1.RNN_1_NET(Data_loaded_train, Data_loaded_test, Data_loaded_val)
			print("\n \n")
			raw_input(" RNN 1 sensor finalizado. Pulse ENTER. ")
			print("\n \n")
		else:
			print("\n")
			print(" No se han cargado los datos. ")
			raw_input("")
			
	if( Opcion_elegida == "6"):
		if (len(Data_loaded_train) != 0) and(len(Data_loaded_test) != 0)and(len( Data_loaded_val) != 0):
			reload(MLP_1)
			MLP_1.MLP_1_NET(Data_loaded_train, Data_loaded_test, Data_loaded_val)
			print("\n \n MLP 1 sensor finalizado. \n \n")
		else:
			print("\n")
			print(" No se han cargado los datos. ")
			raw_input("")
	
	if( Opcion_elegida == "7"):
		if (len(Data_loaded_train) != 0) and(len(Data_loaded_test) != 0)and(len( Data_loaded_val) != 0):
			reload(CNN_1)
			CNN_1.CNN_1_NET(Data_loaded_train, Data_loaded_test, Data_loaded_val)
			print("\n \n")
			raw_input(" CNN 1 sensor finalizado. Pulse ENTER. ")
			print("\n \n")
		else:
			print("\n")
			print(" No se han cargado los datos. ")
			raw_input("")
	if( Opcion_elegida == "8"):
		if (len(Data_loaded_train) != 0) and(len(Data_loaded_test) != 0)and(len( Data_loaded_val) != 0):
			reload(MLP)
			MLP.MLP_NET(Data_loaded_train, Data_loaded_test, Data_loaded_val)
			print("\n \n MLP finalizado. \n \n")
			reload(MLP_1)
			MLP_1.MLP_1_NET(Data_loaded_train, Data_loaded_test, Data_loaded_val)
			print("\n \n MLP 1 sensor finalizado. \n \n")
			reload(CNN)
			CNN.CNN_NET(Data_loaded_train, Data_loaded_test, Data_loaded_val)
			print("\n \n CNN finalizado. \n \n")
			reload(CNN_1)
			CNN_1.CNN_1_NET(Data_loaded_train, Data_loaded_test, Data_loaded_val)
			print("\n \n CNN 1 sensor finalizado. \n \n")
			reload(TestNet)
			TestNet.Test_NET(Data_loaded_train, Data_loaded_test, Data_loaded_val)
			print("\n \n Test sensor finalizado. \n \n")
		else:
			print("\n")
			print(" No se han cargado los datos. ")
			raw_input("")
	if( Opcion_elegida == "9"):
		if (len(Data_loaded_train) != 0) and(len(Data_loaded_test) != 0)and(len( Data_loaded_val) != 0):
			reload(RNN)
			RNN.RNN_NET(Data_loaded_train, Data_loaded_test, Data_loaded_val)
			print("\n \n")
			print(" RNN finalizado. Pulse ENTER. ")
			print("\n \n")
			reload(RNN_1)
			RNN_1.RNN_1_NET(Data_loaded_train, Data_loaded_test, Data_loaded_val)
			print("\n \n")
			print(" RNN 1 sensor finalizado. Pulse ENTER. ")
			print("\n \n")
		else:
			print("\n")
			print(" No se han cargado los datos. ")
			raw_input("")
	if( Opcion_elegida == "10"):
		if (len(Data_loaded_train) != 0) and(len(Data_loaded_test) != 0)and(len( Data_loaded_val) != 0):
			reload(TestNet)
			TestNet.Test_NET(Data_loaded_train, Data_loaded_test, Data_loaded_val)
			print("\n \n Test Net finalizado. \n \n")
		else:
			print("\n")
			print(" No se han cargado los datos. ")
			raw_input("")
	