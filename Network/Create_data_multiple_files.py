from __future__ import print_function
import pandas as pd
import numpy as np
import datetime

def Create_data_multiple_files( directory, Data_type, start_date, end_date ):
	data_range = pd.date_range(start_date, end_date)
	Data_df_list = []
	for date in data_range:
		date_str= date.strftime("%d-%m-%Y")
		dir_file = directory + Data_type + "/" + date_str + "_" + Data_type + ".xlsx"
		Panda_data = pd.read_excel(dir_file)
		Data_df_list.append(Panda_data)
		print("	Archivo procesado", date_str + "_" + Data_type + ".xlsx")
	Data = pd.concat(Data_df_list, ignore_index=True)
	return Data