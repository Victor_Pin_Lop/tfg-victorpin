from __future__ import print_function
import pandas as pd
import numpy as np
import time
from tqdm import tqdm


def distribute_data(filename):
	file = pd.read_csv(filename)
	min_col = "5 Minutes"
	flow_col = "Flow (Veh/5 Minutes)"
	occ_col = "Occupancy (%)"
	speed_col = "Speed (mph)"
	lane_col = "# Lane Points"
	Last_row_1_1 = "12/31/2013 23:55"
	Last_row_1_2 = "12/31/2014 23:55"
	Last_row_1_3 = "12/31/2015 23:55"
	Last_row_1_4 = "12/31/2016 23:55"
	Last_row_2 = "No records to display"
	sensor_long = 1.86
	
	Intenisty_matrix = np.zeros((1))
	Density_matrix = np.zeros((1))
	Speed_matrix = np.zeros((1))
	
	row = 0
	while(True):
		Actual_row = file[min_col][row]
		if(Last_row_2 == Actual_row):
			break
		if (str(file[lane_col][row])=="0") or(str(file[flow_col][row])=="0.0"):
			flow_value = float(flow_imported_value.replace(',',''))*12/float(lane_imported_value.replace(',',''))
			occ_value = float(occ_imported_value.replace(',',''))/float(sensor_long)
			speed_value = float(speed_imported_value.replace(',',''))*1.609
			
			Intenisty_matrix = np.append(Intenisty_matrix, flow_value)
			Density_matrix = np.append(Density_matrix, occ_value)
			Speed_matrix = np.append(Speed_matrix, speed_value)
		else:
			# aparecen errores por la , 1,005.45 -> 1005.45
			flow_imported_value = str(file[flow_col][row])
			lane_imported_value = str(file[lane_col][row])
			occ_imported_value = str(file[occ_col][row])
			speed_imported_value = str(file[speed_col][row])
			
			flow_value = float(flow_imported_value.replace(',',''))*12/float(lane_imported_value.replace(',',''))
			occ_value = float(occ_imported_value.replace(',',''))/float(sensor_long)
			speed_value = float(speed_imported_value.replace(',',''))*1.609
			
			Intenisty_matrix = np.append(Intenisty_matrix, flow_value)
			Density_matrix = np.append(Density_matrix, occ_value)
			Speed_matrix = np.append(Speed_matrix, speed_value)
		row += 1
		if(Last_row_1_1 == Actual_row)or(Last_row_1_2 == Actual_row)or(Last_row_1_3 == Actual_row)or(Last_row_1_4 == Actual_row):
			break
	Intenisty_matrix = np.delete(Intenisty_matrix, 0, axis=0)
	Density_matrix = np.delete(Density_matrix, 0, axis=0)
	Speed_matrix = np.delete(Speed_matrix, 0, axis=0)
	return Intenisty_matrix, Density_matrix, Speed_matrix

