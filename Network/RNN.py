###
# Librerias
###
from __future__ import print_function
import pandas as pd
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt # Plot
import matplotlib.ticker as ticker # Axis
import matplotlib.dates as mdates # Date axis
import datetime # Date object
import time # Process bar
from tqdm import tqdm # Process bar
from subprocess import call # Key detector ( stop loop )

def RNN_NET(Data_input_train, Data_input_test, Data_input_val):
	try:
		# Train
		min_horas_train = int(Data_input_train.shape[1])
		min_sensores = Data_input_train.shape[0]
		
		
		Data_train_original = Data_input_train[:,0:min_horas_train,:]
		Data_train = Data_input_train[:,0:min_horas_train,:]
		# Normalizar entrada
		Data_train = (Data_train - Data_train.mean(axis = 0))/Data_train.std(axis = 0)
		
		# Test
		
		min_horas_test = int(Data_input_test.shape[1])
		Data_test = Data_input_test[:,0:min_horas_test,:]
		Data_test_original = Data_test
		Data_test = (Data_test - Data_test.mean(axis = 0))/Data_test.std(axis = 0)
		
		
		# Validation
		
		min_horas_val = int(Data_input_val.shape[1])
		Data_val = Data_input_val[:,0:min_horas_val,:]
		Data_val_original = Data_val
		Data_val = (Data_val - Data_val.mean(axis = 0))/Data_val.std(axis = 0)
		
		###
		# Variables Red
		###
		batch_size = 15
		pred_hours = 1
		num_channels = 1
		
		# Regularization
		regularizer = 0
		beta = 1
		
		# Cell_num
		Cell_num = 2 #2
		hidden_cell = 40 #40
		prob_cell = 1.0
		activation_cell = tf.nn.relu
		
		# Capas
		W_desv = 0.12
		b_desv = 0.12
		
		hidden_W = 1
		
		# Var optimizer
		start_learning_rate = 1e-4
		stair_steps_learning_rate = 1000
		multiply_learning_rate = 0.1
		max_gradient = 40.
		min_gradient = -40.
		
		# Feed
		num_epochs = 100
		epoch_val = 2
		num_steps = int((min_horas_train - batch_size-pred_hours)/batch_size)
		num_steps_val = int((min_horas_val - batch_size-pred_hours)/batch_size)
		num_steps_test_b = int((min_horas_test - batch_size-pred_hours)/batch_size)
		num_steps_test = min_horas_test-batch_size-pred_hours
		graph = tf.Graph()
		
		with graph.as_default():
			Train_data_ph = tf.placeholder(tf.float32, [min_sensores, batch_size, num_channels])
			Original_data_ph = tf.placeholder(tf.float32, [min_sensores, num_channels])
			
			#https://github.com/guillaume-chevalier/LSTM-Human-Activity-Recognition
			#https://danijar.com/tips-for-training-recurrent-neural-networks/
			
			#Recurrent Dropout without Memory Loss
			#https://arxiv.org/pdf/1603.05118.pdf
			
			# Test
			# https://www.tensorflow.org/api_docs/python/tf/contrib/rnn/Conv2DLSTMCell
			
			def _create_one_cell():
				#return tf.contrib.rnn.BasicRNNCell(num_units=hidden_cell, activation = None)
				#return tf.contrib.rnn.GRUCell(hidden_cell, activation = None)
				#return tf.contrib.rnn.LSTMCell(hidden_cell, activation = activation_cell)
				return tf.contrib.rnn.LayerNormBasicLSTMCell(hidden_cell,dropout_keep_prob = prob_cell, activation = activation_cell)
			if (Cell_num > 1):
				cell = tf.contrib.rnn.MultiRNNCell([_create_one_cell() for _ in range(Cell_num)], state_is_tuple=True)
			else:
				cell = _create_one_cell()
			
			val, state = tf.nn.dynamic_rnn(cell = cell, inputs = Train_data_ph, dtype = tf.float32)
			val = tf.transpose(val, [1,0,2]) ##### ?????
			last = tf.gather(val, int(val.get_shape()[0])-1)
			
			W_1 = tf.Variable(tf.random_normal([hidden_cell, num_channels], stddev = W_desv))
			b_1 = tf.Variable(tf.zeros([num_channels]), dtype = tf.float32)
			
			Capa_1 =  tf.matmul(last, W_1) + b_1
			
			prediction = Capa_1
			
			#loss = tf.nn.l2_loss(prediction-Original_data_ph)
			loss = (0.5)*(tf.norm(prediction-Original_data_ph))**2
			MAE = tf.reduce_mean(tf.abs(prediction - Original_data_ph))
			MAPE = tf.reduce_mean(tf.abs((prediction - Original_data_ph)/Original_data_ph))
			
			regularizer = 0
			regularizer = regularizer + tf.nn.l2_loss(W_1) + tf.nn.l2_loss(b_1)
			
			loss_b = loss + beta*regularizer
			
			# Optimizer GradientDescent + Exponential + Gradiente capado ( v1 )
			global_step_lr = tf.Variable(0)
			learning_rate = tf.train.exponential_decay(start_learning_rate, global_step_lr, stair_steps_learning_rate, multiply_learning_rate, staircase=True)
			optimizer = tf.train.GradientDescentOptimizer(learning_rate)
			gradients = optimizer.compute_gradients(loss_b)
			def ClipIfNotNone(grad):
				if grad is None:
					return grad
				return tf.clip_by_value(grad, min_gradient, max_gradient)
			clipped_gradients = [(ClipIfNotNone(grad), var) for grad, var in gradients]
			train = optimizer.apply_gradients(clipped_gradients, global_step=global_step_lr)
			
			correct_prediction = tf.equal( tf.round( prediction ), tf.round( Original_data_ph ) )
			accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
		
		
		
		with tf.Session(graph=graph) as sess:
			tf.global_variables_initializer().run()
			
			# Plot 
			Sensor_to_plot = 5
			Var_to_plot = 1 # [0,1,2] "Intensity, Speed, Density"
			
			#########
			# TRAIN & VALIDATION
			#########
			
			Total_loss_train = np.zeros((num_epochs))
			Total_loss_val = np.zeros((int((num_epochs)/epoch_val)))
			val_epoch = 0
			
			# TRAIN var
			Epoch_loss_train = np.zeros((num_steps))
			Epoch_mae_train = np.zeros((num_steps))
			Epoch_mape_train = np.zeros((num_steps))
			Epoch_accuracy_train = np.zeros((num_steps))
			
			# VALIDATION var
			Epoch_loss_val = np.zeros((num_steps))
			Epoch_mae_val = np.zeros((num_steps))
			Epoch_mape_val = np.zeros((num_steps))
			Epoch_accuracy_val = np.zeros((num_steps))
			
			print(' \n \n-- -- Running RNN -- -- \n \n ')
			for epoch in range(num_epochs):
				batch_data = np.zeros((min_sensores, batch_size, num_channels))
				original_data = np.zeros((min_sensores, num_channels))
				
				print(' -- Epoch ', epoch)
				
				print(' - TRAIN - ')
				
				for step in tqdm(range(num_steps)):
				#for step in tqdm(np.random.permutation(num_steps)):
					try:
						offset = (step*batch_size) % (min_horas_train - batch_size)
						
						batch_data[:,:,0] = Data_train[:,offset:offset+batch_size,Var_to_plot]
						original_data[:,0] = Data_train_original[:,offset+batch_size+pred_hours,Var_to_plot]
						
					
						feed_dict = {Train_data_ph : batch_data, Original_data_ph : original_data}
						_,loss_train, MAE_train, MAPE_train, acc_train = sess.run([train, loss, MAE, MAPE, accuracy], feed_dict = feed_dict)
						
						Epoch_loss_train[step] = np.mean(loss_train)
						Epoch_mae_train[step] = MAE_train
						Epoch_mape_train[step] = MAPE_train*100
						Epoch_accuracy_train[step] = acc_train*100
						
						time.sleep(0.0001)
					except KeyboardInterrupt:
						break
				Total_loss_train[epoch] = np.mean(Epoch_loss_train)
				
				print(' - Train MAE ', np.mean(Epoch_mae_train))
				
				# VALIDATION 
				
				if((epoch-1)%epoch_val==0)and(epoch!=0):
					
					print(' - VALIDATION - ')
					
					for step in tqdm(range(num_steps_val)):
					#for step in tqdm(np.random.permutation(num_steps_val)):
						try:
							offset = (step*batch_size) % (min_horas_train - batch_size)
							
							batch_data[:,:,0] = Data_val[:,offset:offset+batch_size,Var_to_plot]
							original_data[:,0] = Data_val_original[:,offset+batch_size+pred_hours,Var_to_plot]
							
							feed_dict = {Train_data_ph : batch_data, Original_data_ph : original_data}
							loss_val, MAE_val, MAPE_val, acc_val = sess.run([loss, MAE, MAPE, accuracy], feed_dict = feed_dict)
							
							Epoch_loss_val[step] = np.mean(loss_val)
							Epoch_mae_val[step] = MAE_val
							Epoch_mape_val[step] = MAPE_val*100
							Epoch_accuracy_val[step] = acc_val*100
							
							time.sleep(0.0001)
						except KeyboardInterrupt:
							break
					Total_loss_val[val_epoch] = np.mean(Epoch_loss_val)
					val_epoch += 1
					print(' - Val MAE ', np.mean(Epoch_mae_val))
				
			
			print(" --- TRAIN ---")
			print(" - Last epoch MAE ", np.mean(Epoch_mae_train))
			print(" - Last epoch MAPE ", np.mean(Epoch_mape_train))
			print(' - Last epoch accuracy ' , np.mean(Epoch_accuracy_train))
			print(' - Last epoch loss ' , np.mean(Epoch_loss_train))
			print(' - Total train loss ' , np.mean(Total_loss_train))
			
			print(" --- VALIDATION ---")
			print(" - Last epoch MAE ", np.mean(Epoch_mae_val))
			print(" - Last epoch MAPE ", np.mean(Epoch_mape_val))
			print(' - Last epoch accuracy ' , np.mean(Epoch_accuracy_val))
			print(' - Last epoch loss ' , np.mean(Epoch_loss_val))
			print(' - Total validation loss ' , np.mean(Total_loss_val))
			
			#########
			# To PLOT Loss
			#########
			
			if True:
				
				plt.figure()
				Plot_total_loss = './plots/RNN_Total_loss.png'
				
				train = Total_loss_train
				val = Total_loss_val
				n = int(len(train)/len(val))
				
				plt.plot(train, label='Training loss')
				plt.plot([x + n - 1 for x in range(0, len(train), n)], val, '--', label='Validation loss')
				plt.legend(fontsize=6)
				plt.ylabel('Loss')
				plt.xlabel('Epocas')
				
				#plt.show()
				plt.savefig(Plot_total_loss, dpi = 800)
			
			
			#########
			# To TXT
			#########
			
			MAE_train_txt = np.reshape(np.mean(Epoch_mae_train),(1,1))
			MAPE_train_txt = np.reshape(np.mean(Epoch_mape_train),(1,1))
			Total_loss_train_txt = np.reshape(np.mean(Total_loss_train),(1,1))
			Total_acc_train_txt = np.reshape(np.mean(Epoch_accuracy_train),(1,1))
			
			np.savetxt("./plots/RNN_MAE_train.txt",MAE_train_txt, delimiter='\n')
			np.savetxt("./plots/RNN_MAPE_train.txt",MAPE_train_txt, delimiter='\n')
			np.savetxt("./plots/RNN_Last_Total_Loss_train.txt",Total_loss_train_txt, delimiter='\n')
			np.savetxt("./plots/RNN_Last_Total_acc_train.txt",Total_acc_train_txt, delimiter='\n')
			
			MAE_val_txt = np.reshape(np.mean(Epoch_mae_val),(1,1))
			MAPE_val_txt = np.reshape(np.mean(Epoch_mape_val),(1,1))
			Total_loss_val_txt = np.reshape(np.mean(Total_loss_val),(1,1))
			Total_acc_val_txt = np.reshape(np.mean(Epoch_accuracy_val),(1,1))
			
			np.savetxt("./plots/RNN_MAE_val.txt",MAE_val_txt, delimiter='\n')
			np.savetxt("./plots/RNN_MAPE_val.txt",MAPE_val_txt, delimiter='\n')
			np.savetxt("./plots/RNN_Last_Total_loss_val.txt",Total_loss_val_txt, delimiter='\n')
			np.savetxt("./plots/RNN_Last_Total_acc_val.txt",Total_acc_val_txt, delimiter='\n')
			
			
			#########
			# TEST 
			#########
			
			if True:
				# Plot 
				Sensor_to_plot = 5
				Var_to_plot = 1 # [0,1,2] "Intensity, Speed, Density"
				
				original_data = np.zeros((min_sensores,1))
				batch_data = np.zeros((min_sensores, batch_size, num_channels))
				Epoch_pred_test = np.zeros((min_sensores, num_steps_test))
				
				Epoch_loss_test = np.zeros((num_steps_test_b))
				Epoch_mae_test = np.zeros((num_steps_test_b))
				Epoch_mape_test = np.zeros((num_steps_test_b))
				Epoch_accuracy_test = np.zeros((num_steps_test_b))
				
				print(' - TESTING - ')
				
				for step in (range(num_steps_test)):
				#for step in tqdm(np.random.permutation(num_steps_val)):
					try:
						#offset = (step*batch_size) % (min_horas_test - batch_size)
						offset = step+batch_size
						
						batch_data[:,:,0] = Data_test[:,step:offset,Var_to_plot]
						original_data[:,0] = Data_test_original[:,offset+pred_hours,Var_to_plot]
						
						feed_dict = {Train_data_ph : batch_data, Original_data_ph : original_data}
						loss_test, MAE_test, MAPE_test, acc_test, pred_test = sess.run([loss, MAE, MAPE, accuracy, prediction], feed_dict = feed_dict)
						
						Epoch_pred_test[:,step] = pred_test[:,0]
						
						time.sleep(0.0001)
					except KeyboardInterrupt:
						break
				
				for step in (range(num_steps_test_b)):
				#for step in tqdm(np.random.permutation(num_steps_test_b)):
					try:
						offset = (step*batch_size) % (min_horas_test - batch_size)
						#offset = step+batch_size
						
						batch_data[:,:,0] = Data_test[:,offset:offset+batch_size,Var_to_plot]
						original_data[:,0] = Data_test_original[:,offset+batch_size+pred_hours,Var_to_plot]
						
						feed_dict = {Train_data_ph : batch_data, Original_data_ph : original_data}
						loss_test, MAE_test, MAPE_test, acc_test = sess.run([loss, MAE, MAPE, accuracy], feed_dict = feed_dict)
						
						Epoch_loss_test[step] = np.mean(loss_test)
						Epoch_mae_test[step] = MAE_test
						Epoch_mape_test[step] = MAPE_test*100
						Epoch_accuracy_test[step] = acc_test*100
						
						time.sleep(0.0001)
					except KeyboardInterrupt:
						break
				
				print(" --- Testing ---")
				print(" - Epoch MAE ", np.mean(Epoch_mae_test))
				print(" - Epoch MAPE ", np.mean(Epoch_mape_test))
				print(' - Epoch accuracy ' , np.mean(Epoch_accuracy_test))
				print(' - Epoch loss ' , np.mean(Epoch_loss_test))
				
				#########
				# To PLOT Prediction
				#########
				
				if True:
					datemin = datetime.datetime(2013, 1, 1, 0, 0)
					datemax = datetime.datetime(2013, 12, 31, 23, 0)
					plot_save_PRED = './plots/RNN_Test_PRED.png'
					plt.figure()
					y = Data_test_original[Sensor_to_plot,:,Var_to_plot]
					x = [datemin + datetime.timedelta(minutes=i*5) for i in range(len(y))]
					y_p = Epoch_pred_test[Sensor_to_plot, :]
					y = y[76275+batch_size+pred_hours:77727+batch_size+pred_hours]
					x = x[76275+batch_size+pred_hours:77727+batch_size+pred_hours]
					y_p = y_p[76275:77727]
					plt.xticks(x, rotation='vertical',fontsize=6)
					plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%A - %H:%M'))
					plt.gca().xaxis.set_major_locator(mdates.HourLocator(interval = 2))
					plt.gcf().autofmt_xdate()
					plt.ylabel('Velocidad en el sensor [km/h]',fontsize=12)
					plt.xlabel('Dia - Hora',fontsize=12)
					plt.plot(x,y, label='Velocidad original')
					plt.plot(x,y_p, label = 'Velocidad predicha')
					plt.legend(fontsize=6)
					plt.tight_layout()
				#	plt.show()
					plt.savefig(plot_save_PRED,dpi=800)
				
				#########
				# To PLOT Loss
				#########
				
				if False:
					
					plt.figure()
					Plot_total_loss = './plots/RNN_Epoch_loss.png'
					
					train = Epoch_loss_train
					val = Epoch_loss_val
					test = Epoch_loss_test
					
					n = int(len(train)/len(val))
					m = int(len(train)/len(test))
					
					plt.plot(train)
					plt.plot([x + n - 1 for x in range(0, len(train), n)], val, '--')
					plt.plot([x + m - 1 for x in range(0, len(train), m)], test, '-.', color = 'red')
					#plt.show()
					plt.savefig(Plot_total_loss, dpi = 800)
				
				#########
				# To TXT
				#########
				
				if True:
					MAE_test_txt = np.reshape(np.mean(Epoch_mae_test),(1,1))
					MAPE_test_txt = np.reshape(np.mean(Epoch_mape_test),(1,1))
					Total_acc_test_txt = np.reshape(np.mean(Epoch_accuracy_test),(1,1))
					Total_loss_test_txt = np.reshape(np.mean(Epoch_loss_test),(1,1))
					
					np.savetxt("./plots/RNN_MAE_test.txt",MAE_test_txt, delimiter='\n')
					np.savetxt("./plots/RNN_MAPE_test.txt",MAPE_test_txt, delimiter='\n')
					np.savetxt("./plots/RNN_Last_Total_acc_test.txt",Total_acc_test_txt, delimiter='\n')
					np.savetxt("./plots/RNN_Last_Total_Loss_test.txt",Total_loss_test_txt, delimiter='\n')
			
				
				
	except ValueError as Error:
		print("Error: ", Error)
	return 




