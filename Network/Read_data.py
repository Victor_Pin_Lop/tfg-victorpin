from __future__ import print_function
import pandas as pd
import numpy as np
import create_data as cd
import matplotlib.pyplot as plt # Plot
import matplotlib.ticker as ticker # Axis
import matplotlib.dates as mdates # Date axis
import datetime # Date object

def Read_data(Data_train, Data_test, Data_val):
	
	###
	# Directorios y archivos
	###
	
	Dir_multiple_files_train = './Downloaded files Guill/'
	Dir_multiple_files_val = './Downloaded files val Guill/'
	Dir_multiple_files_test = './Downloaded files test Guill/'
	
	Intensity_read_train = Dir_multiple_files_train+'Intensity_BIG_Guill.xlsx'
	Speed_read_train = Dir_multiple_files_train+'Speed_BIG_Guill.xlsx'
	Density_read_train = Dir_multiple_files_train+'Density_BIG_Guill.xlsx'
	
	Intensity_read_val = Dir_multiple_files_val+'Intensity_BIG_Guill.xlsx'
	Speed_read_val = Dir_multiple_files_val+'Speed_BIG_Guill.xlsx'
	Density_read_val = Dir_multiple_files_val+'Density_BIG_Guill.xlsx'
	
	Intensity_read_test = Dir_multiple_files_test+'Intensity_BIG_Guill.xlsx'
	Speed_read_test = Dir_multiple_files_test+'Speed_BIG_Guill.xlsx'
	Density_read_test = Dir_multiple_files_test+'Density_BIG_Guill.xlsx'
	
	###
	#	Lectura, procesado y save de datos
	###
	
	# Variables menu
	Opcion = ""
	
	# Menu
	print("\n")
	print("\n")
	print(" -- -- Lectura y procesado de datos -- -- ")
	print("\n")
	print("\n")
	
	print(" 1 - Leer la matriz desde un archivo guardado ")
	print("\n")
	print(" 2 - Cargar datos desde multiples archivos ")
	print("\n")
	print(" 3 - Caracteristicas de los datos de train ( media, distribucion y varianza ) ")
	print("\n")
	print(" 4 - Mostrar graficas de velocidad de todos los sensores para train. ")
	print("\n")
	print(" 5 - Volver ")
	print("\n")
	print("\n")
	
	# Acciones a realizar
	while(True):
		Opcion = raw_input('  Escoja una opcion ')
		if  ( Opcion == "1" ) or ( Opcion == "2" )or( Opcion == "3" )or( Opcion == "4" )or( Opcion == "5" ):
			break 
	
	if(Opcion == "1"):
			print(" \n \n -- -- Cargando matriz desde archivo ( train y test )-- -- \n \n ")
			
			# Train
			
			panda_data = pd.read_excel(Intensity_read_train, sheet_name='Sheet')
			Data_intensity_train = panda_data.values.astype(np.float32)
			
			panda_data = pd.read_excel(Speed_read_train, sheet_name='Sheet')
			Data_speed_train = panda_data.values.astype(np.float32)
			
			panda_data = pd.read_excel(Density_read_train, sheet_name='Sheet')
			Data_density_train = panda_data.values.astype(np.float32)
			
			min_horas_intensity_train = Data_intensity_train.shape[1]
			Num_sensors_intensity_train = Data_intensity_train.shape[0]
			
			min_horas_speed_train = Data_speed_train.shape[1]
			Num_sensors_speed_train = Data_speed_train.shape[0]
			
			min_horas_density_train = Data_density_train.shape[1]
			Num_sensors_density_train = Data_density_train.shape[0]
			
			# Validation
			
			panda_data = pd.read_excel(Intensity_read_val, sheet_name='Sheet')
			Data_intensity_val = panda_data.values.astype(np.float32)
			#Data_intensity_val = Data_intensity_train
			
			panda_data = pd.read_excel(Speed_read_val, sheet_name='Sheet')
			Data_speed_val = panda_data.values.astype(np.float32)
			#Data_speed_val = Data_speed_train
			
			panda_data = pd.read_excel(Density_read_val, sheet_name='Sheet')
			Data_density_val = panda_data.values.astype(np.float32)
			#Data_density_val = Data_density_train
			
			min_horas_intensity_val = int(Data_intensity_val.shape[1])
			Num_sensors_intensity_val = Data_intensity_val.shape[0]
			
			min_horas_speed_val = int(Data_speed_val.shape[1])
			Num_sensors_speed_val = Data_speed_val.shape[0]
			
			min_horas_density_val = int(Data_density_val.shape[1])
			Num_sensors_density_val = Data_density_val.shape[0]
			
			# Validation
			
			panda_data = pd.read_excel(Intensity_read_test, sheet_name='Sheet')
			Data_intensity_test = panda_data.values.astype(np.float32)
			#Data_intensity_test = Data_intensity_train
			
			panda_data = pd.read_excel(Speed_read_test, sheet_name='Sheet')
			Data_speed_test = panda_data.values.astype(np.float32)
			#Data_speed_test = Data_speed_train
			
			panda_data = pd.read_excel(Density_read_test, sheet_name='Sheet')
			Data_density_test = panda_data.values.astype(np.float32)
			#Data_density_test = Data_density_train
			
			min_horas_intensity_test = int(Data_intensity_test.shape[1])
			Num_sensors_intensity_test = Data_intensity_test.shape[0]
			
			min_horas_speed_test = int(Data_speed_test.shape[1])
			Num_sensors_speed_test = Data_speed_test.shape[0]
			
			min_horas_density_test = int(Data_density_test.shape[1])
			Num_sensors_density_test = Data_density_test.shape[0]
			
			
			del panda_data
	
	if(Opcion == '2'):
		print(" \n \n -- -- Cargando y guardando datos desde multiples archivos -- -- \n \n ")
		# DATE : YYYY-MM-DD
		sensor_array = [1118333,1114709,1118348,1114720,1118352,1118743,1118379,1114734,1123135,1123133,1120362,1114748,1118796,1118421,1118735,1118450,1120597,1114190,1123331,1118458,1118479,1114205,1125469,1114211,1117762,1114219,1117782,1114100,1117796,1117809,1114276,1117748,1113976,1117741,1108603,1117835,1118078,1117717,1108611,1108613,1114050,1108617,1115269,1118496,1114805,1114817,1114035,1108547,1118707,1118521,1108665,1114290,1119637,1122594,1118537,1114296,1118544,1108719,1123086,1114831,1122528,1123081,1123078,1114659,1122536,1118170,1122552,1114847,1122575,1108437,1115277,1119828,1108507,1119679,1119850,1108509,1108512,1119645,1108514,1122507,1119865,1125314,1119871,1122512,1108651,1119890,1108663,1119903,1108682,1119921,1108687,1108659,1119928,1119934,1119947,1115289,1115304]		
		print(' Train ')
		'''
		[Data_intensity_train, Num_sensors_intensity_train, min_horas_intensity_train, 
		 Data_speed_train, Num_sensors_speed_train, min_horas_speed_train, 
		 Data_density_train, Num_sensors_density_train, min_horas_density_train] = cd.Create_from_multi_files(Dir_multiple_files_train, sensor_array)
		'''
		print(' Validation ')
		'''
		[Data_intensity_val, Num_sensors_intensity_val, min_horas_intensity_val, 
		 Data_speed_val, Num_sensors_speed_val, min_horas_speed_val, 
		 Data_density_val, Num_sensors_density_val, min_horas_density_val] = cd.Create_from_multi_files(Dir_multiple_files_val, sensor_array)
		'''
		print(' Testing ')
		'''
		[Data_intensity_test, Num_sensors_intensity_test, min_horas_intensity_test, 
		 Data_speed_test, Num_sensors_speed_test, min_horas_speed_test, 
		 Data_density_test, Num_sensors_density_test, min_horas_density_test] = cd.Create_from_multi_files(Dir_multiple_files_test, sensor_array)
		'''
		print(' - - - funcion desactivada - - - ' )
	
	if(Opcion == '3'):
		print(' \n \n -- -- Caracteristicas de los datos train -- -- \n \n ')
		
		print(' Datos sin normalizar ')
		
		# Media
		Media_intensity_train = np.mean(Data_train[:,:,0])
		Media_speed_train = np.mean(Data_train[:,:,1])
		Media_density_train = np.mean(Data_train[:,:,2])
		
		# Varianza
		Var_intensity_train = np.var(Data_train[:,:,0])
		Var_speed_train = np.var(Data_train[:,:,1])
		Var_density_train = np.var(Data_train[:,:,2])
		
		# Distribucion
		Dist_data_intensity_train = np.hstack(Data_train[:,:,0])
		Dist_data_speed_train = np.hstack(Data_train[:,:,1])
		Dist_data_density_train = np.hstack(Data_train[:,:,2])
		
		print('\n')
		print('Media intensidad : ', Media_intensity_train) 
		print('Media velocidad : ', Media_speed_train)
		print('Media densidad : ', Media_density_train)
		print('\n')
		print('Varianza intensidad : ', Var_intensity_train) 
		print('Varianza velocidad : ', Var_speed_train)
		print('Varianza densidad : ', Var_density_train)
		print('\n')
		
		plt.figure()
		plt.title('Distribucion Intensidad')
		plt.hist(Dist_data_intensity_train, bins='auto')
		plt.ylabel('Vehiculos entre carriles por hora')
		plt.show()
		
		plt.figure()
		plt.title('Distribucion Velocidad Media')
		plt.hist(Dist_data_speed_train, bins='auto')
		plt.ylabel('Velocidad media ( mill/h )')
		plt.show()
		
		plt.figure()
		plt.title('Distribucion Densidad')
		plt.hist(Dist_data_intensity_train, bins='auto')
		plt.ylabel('Cantidad de vhiculos entre sensores')
		plt.show()
		
		
		print(' Datos normalizados ')
		
		Data_train_n = (Data_train - Data_train.mean(axis=0))/(Data_train.std(axis=0))
		
		# Media
		Media_intensity_train = np.mean(Data_train_n[:,:,0])
		Media_speed_train = np.mean(Data_train_n[:,:,1])
		Media_density_train = np.mean(Data_train_n[:,:,2])
		
		# Varianza
		Var_intensity_train = np.var(Data_train_n[:,:,0])
		Var_speed_train = np.var(Data_train_n[:,:,1])
		Var_density_train = np.var(Data_train_n[:,:,2])
		
		# Distribucion
		Dist_data_intensity_train = np.hstack(Data_train_n[:,:,0])
		Dist_data_speed_train = np.hstack(Data_train_n[:,:,1])
		Dist_data_density_train = np.hstack(Data_train_n[:,:,2])
		
		print('\n')
		print('Media intensidad : ', Media_intensity_train) 
		print('Media velocidad : ', Media_speed_train)
		print('Media densidad : ', Media_density_train)
		print('\n')
		print('Varianza intensidad : ', Var_intensity_train) 
		print('Varianza velocidad : ', Var_speed_train)
		print('Varianza densidad : ', Var_density_train)
		print('\n')
		
		plt.figure()
		plt.title('Distribucion Intensidad')
		plt.hist(Dist_data_intensity_train, bins='auto')
		plt.ylabel('Vehiculos entre carriles por hora')
		plt.show()
		
		plt.figure()
		plt.title('Distribucion Velocidad Media')
		plt.hist(Dist_data_speed_train, bins='auto')
		plt.ylabel('Velocidad media ( mill/h )')
		plt.show()
		
		plt.figure()
		plt.title('Distribucion Densidad')
		plt.hist(Dist_data_intensity_train, bins='auto')
		plt.ylabel('Cantidad de vhiculos entre sensores')
		plt.show()
		
		print('\n')
		print('\n')
		raw_input(' Datos procesados y caracteristicas mostradas.')
		print('\n')
		
		return Data_train, Data_test, Data_val
	
	
	if(Opcion == '4'):
		# 9 al 12, 14, 18 al 21
		for sensor in range(1):
			#sens = 6
			titulo = 'Sensor '+ str(sensor + 1)
			datemin = datetime.datetime(2013, 1, 1, 0, 0)
			datemax = datetime.datetime(2013, 12, 31, 23, 0)
			plt.figure()
			plt.title(titulo)
			y = Data_test[5,:,1]
			x = [datemin + datetime.timedelta(minutes=i*5) for i in range(len(y))]
			y = y[76275:77727]
			x = x[76275:77727]
			plt.xticks(x, rotation='vertical',fontsize=6)
			plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%A - %H:%M'))
			plt.gca().xaxis.set_major_locator(mdates.HourLocator(interval = 2))
			plt.gcf().autofmt_xdate()
			plt.ylabel('Velocidad en el sensor [km/h]',fontsize=12)
			plt.xlabel('Dia - Hora',fontsize=12)
			plt.plot(x,y)
			plt.tight_layout()
			#plt.show()
			plt.savefig("Sensor_6_hours.png",dpi=800)
		return Data_train, Data_test, Data_val
	
	if(Opcion == '5'):
		return Data_train, Data_test, Data_val
	
	# Train
	
	print('Minimo de horas de las fuentes para Train : ')
	print(' - Intensidad : ', min_horas_intensity_train)
	print(' - Velocidad med. : ', min_horas_speed_train)
	print(' - Densidad : ', min_horas_density_train)
	print('\n')
	print('Minimo de sensores de las fuentes para Train : ')
	print(' - Intensidad : ', Num_sensors_intensity_train)
	print(' - Velocidad med. : ', Num_sensors_speed_train)
	print(' - Densidad : ', Num_sensors_density_train)
	print('\n')
	
	min_horas_train = np.min([min_horas_intensity_train, min_horas_speed_train, min_horas_density_train])
	min_sensores_train = np.min([Num_sensors_intensity_train, Num_sensors_speed_train, Num_sensors_density_train])
	
	print(' Minimo de horas detectadas en Train : ' , min_horas_train)
	
	# Partir datos para Train y Test y creamos la imagen (min_sensores x min_horas_train x 3)
	
	Data_intensity_train = Data_intensity_train[0:min_sensores_train,0:min_horas_train]
	Data_speed_train = Data_speed_train[0:min_sensores_train,0:min_horas_train]
	Data_density_train = Data_density_train[0:min_sensores_train,0:min_horas_train]
	
	Data_train = np.zeros((min_sensores_train, min_horas_train,3))
	Data_train[0:min_sensores_train, 0:min_horas_train,0] = Data_intensity_train
	Data_train[0:min_sensores_train, 0:min_horas_train,1] = Data_speed_train
	Data_train[0:min_sensores_train, 0:min_horas_train,2] = Data_density_train
	
	print("\n")
	print("\n")
	
	# Validation
	
	min_sensores_val = np.min([Num_sensors_intensity_val, Num_sensors_speed_val, Num_sensors_density_val])
	
	Data_intensity_val = Data_intensity_val[0:min_sensores_val,0:min_horas_intensity_val]
	Data_speed_val = Data_speed_val[0:min_sensores_val,0:min_horas_speed_val]
	Data_density_val = Data_density_val[0:min_sensores_val,0:min_horas_density_val]
	
	min_horas_val = np.min([Data_intensity_val.shape[1], Data_speed_val.shape[1], Data_density_val.shape[1]])
	
	Data_val = np.zeros((min_sensores_val, min_horas_val,3))
	Data_val[0:min_sensores_val, 0:min_horas_val,0] = Data_intensity_val
	Data_val[0:min_sensores_val, 0:min_horas_val,1] = Data_speed_val
	Data_val[0:min_sensores_val, 0:min_horas_val,2] = Data_density_val
	
	print('Minimo de horas de las fuentes para Valicacion : ')
	print(' - Intensidad : ', min_horas_intensity_val)
	print(' - Velocidad med. : ', min_horas_speed_val)
	print(' - Densidad : ', min_horas_density_val)
	print('\n')
	print('Minimo de sensores de las fuentes para Validacion : ')
	print(' - Intensidad : ', Num_sensors_intensity_val)
	print(' - Velocidad med. : ', Num_sensors_speed_val)
	print(' - Densidad : ', Num_sensors_density_val)
	print('\n')
	
	print(' Minimo de horas detectadas en Validacion : ' , min_horas_val)
	
	print("\n")
	print("\n")
	
	# Testing
	
	min_sensores_test = np.min([Num_sensors_intensity_test, Num_sensors_speed_test, Num_sensors_density_test])
	
	Data_intensity_test = Data_intensity_test[0:min_sensores_test,0:Data_intensity_test.shape[1]]
	Data_speed_test = Data_speed_test[0:min_sensores_test,0:Data_speed_test.shape[1]]
	Data_density_test = Data_density_test[0:min_sensores_test,0:Data_density_test.shape[1]]
	
	min_horas_test = np.min([Data_intensity_test.shape[1], Data_speed_test.shape[1], Data_density_test.shape[1]])
	
	Data_test = np.zeros((min_sensores_test, min_horas_test,3))
	Data_test[0:min_sensores_test, 0:min_horas_test,0] = Data_intensity_test
	Data_test[0:min_sensores_test, 0:min_horas_test,1] = Data_speed_test
	Data_test[0:min_sensores_test, 0:min_horas_test,2] = Data_density_test
	
	print('Minimo de horas de las fuentes para Testing : ')
	print(' - Intensidad : ', min_horas_intensity_test)
	print(' - Velocidad med. : ', min_horas_speed_test)
	print(' - Densidad : ', min_horas_density_test)
	print('\n')
	print('Minimo de sensores de las fuentes para Testing : ')
	print(' - Intensidad : ', Num_sensors_intensity_test)
	print(' - Velocidad med. : ', Num_sensors_speed_test)
	print(' - Densidad : ', Num_sensors_density_test)
	print('\n')
	
	print(' Minimo de horas detectadas en Testing : ' , min_horas_test)
	
	print("\n")
	print("\n")
	
	
	print(" Datos procesados." )
	
	return Data_train, Data_test, Data_val
