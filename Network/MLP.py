###
# Librerias
###
from __future__ import print_function
import pandas as pd # Animal
import tensorflow as tf # Dunno
import numpy as np # Math
import matplotlib.pyplot as plt # Plot
import matplotlib.ticker as ticker # Axis
import matplotlib.dates as mdates # Date axis
import datetime # Date object
import time # Process bar
from tqdm import tqdm # Process bar
from subprocess import call # Key detector ( stop loop )

def MLP_NET(Data_input_train, Data_input_test, Data_input_val):
	try:
		
		# Train
		min_horas_train = int(Data_input_train.shape[1])
		min_sensores = Data_input_train.shape[0]
		
		Data_train_original = Data_input_train[:,0:min_horas_train,:]
		Data_train = Data_input_train[:,0:min_horas_train,:]
		# Normalizar entrada
		Data_train = (Data_train - Data_train.mean(axis = 0))/(Data_train.std(axis = 0))
		
		# Test
		
		min_horas_test = int(Data_input_test.shape[1])
		Data_test = Data_input_test[:,0:min_horas_test,:]
		Data_test_original = Data_test
		Data_test = (Data_test - Data_test.mean(axis = 0))/Data_test.std(axis = 0)
		
		
		# Validation
		
		min_horas_val = int(Data_input_val.shape[1])
		Data_val = Data_input_val[:,0:min_horas_val,:]
		Data_val_original = Data_val
		Data_val = (Data_val - Data_val.mean(axis = 0))/Data_val.std(axis = 0)
		
		###
		# Variables Red
		###
		
		batch_size = 50
		num_channels = 1
		pred_hours = 3
		bn_train = True
		UseXavier = False
		
		# Regularizer
		regularizer = 0
		beta = 1
		
		# Learning rate
		start_learning_rate = 1e-4
		stair_steps_learning_rate = 800
		multiply_learning_rate = 0.1
		
		# Optimizer
		max_gradient = 40.
		min_gradient = -40.
		
		# Capas
		hidden_1 = 4096
		hidden_2 = 3072
		hidden_3 = 3072
		hidden_4 = 3072
		hidden_5 = 2048
		hidden_6 = 2048
		hidden_7 = 2048
		hidden_8 = 2048
		hidden_9 = 1024
		hidden_10 = 1024
		hidden_11 = 1024
		hidden_FCf = num_channels
		
		W_desv = 0.1222
		b_desv = 0.1222
		
		# Feed
		num_epochs = 20
		epoch_val = 2
		num_steps = int((min_horas_train - batch_size-pred_hours)/batch_size)
		num_steps_val = int((min_horas_val - batch_size-pred_hours)/batch_size)
		num_steps_test_b = int((min_horas_test - batch_size-pred_hours)/batch_size)
		num_steps_test = min_horas_test - batch_size - pred_hours
		graph = tf.Graph()
		
		
		# Definir Red
		with graph.as_default():
			Train_data_ph = tf.placeholder(tf.float32, shape=(min_sensores, batch_size ))
			Original_data_ph = tf.placeholder(tf.float32, shape=(min_sensores, num_channels ))
			
			if ( UseXavier == True ):
				# https://www.tensorflow.org/api_docs/python/tf/contrib/layers/xavier_initializer
				# Capa 1
				W_1_1 = tf.get_variable( 'W_1_1',shape=[ batch_size, hidden_1 ],initializer=tf.contrib.layers.xavier_initializer())
				b_1_1 = tf.get_variable( 'b_1_1',shape=[ hidden_1 ],initializer=tf.contrib.layers.xavier_initializer())
				W_1_2 = tf.get_variable( 'W_1_2',shape=[ batch_size, hidden_1 ],initializer=tf.contrib.layers.xavier_initializer())
				b_1_2 = tf.get_variable( 'b_1_2',shape=[ hidden_1 ],initializer=tf.contrib.layers.xavier_initializer())
				W_1_3 = tf.get_variable( 'W_1_3',shape=[ batch_size, hidden_1 ],initializer=tf.contrib.layers.xavier_initializer())
				b_1_3 = tf.get_variable( 'b_1_3',shape=[ hidden_1 ],initializer=tf.contrib.layers.xavier_initializer())
				
				# Capa 2
				W_2 = tf.get_variable( 'W_2',shape=[ hidden_1, hidden_2 ],initializer=tf.contrib.layers.xavier_initializer())
				b_2 = tf.get_variable( 'b_2',shape=[ hidden_2 ],initializer=tf.contrib.layers.xavier_initializer())
				
				# Capa 3
				W_3 = tf.get_variable( 'W_3',shape=[ hidden_2, hidden_3 ],initializer=tf.contrib.layers.xavier_initializer())
				b_3 = tf.get_variable( 'b_3',shape=[ hidden_3 ],initializer=tf.contrib.layers.xavier_initializer())
				
				# Capa 4
				W_4 = tf.get_variable( 'W_4',shape=[ hidden_3, hidden_4 ],initializer=tf.contrib.layers.xavier_initializer())
				b_4 = tf.get_variable( 'b_4',shape=[ hidden_4 ],initializer=tf.contrib.layers.xavier_initializer())
				
				# Capa 5
				W_5 = tf.get_variable( 'W_5',shape=[ hidden_4, hidden_5 ],initializer=tf.contrib.layers.xavier_initializer())
				b_5 = tf.get_variable( 'b_5',shape=[ hidden_5 ],initializer=tf.contrib.layers.xavier_initializer())
				
				# Capa 6
				W_6 = tf.get_variable( 'W_6',shape=[ hidden_5, hidden_6 ],initializer=tf.contrib.layers.xavier_initializer())
				b_6 = tf.get_variable( 'b_6',shape=[ hidden_6 ],initializer=tf.contrib.layers.xavier_initializer())
				
				# Capa 7
				W_7 = tf.get_variable( 'W_7',shape=[ hidden_6, hidden_7 ],initializer=tf.contrib.layers.xavier_initializer())
				b_7 = tf.get_variable( 'b_7',shape=[ hidden_7 ],initializer=tf.contrib.layers.xavier_initializer())
				
				# Capa 8
				W_8 = tf.get_variable( 'W_8',shape=[ hidden_7, hidden_8 ],initializer=tf.contrib.layers.xavier_initializer())
				b_8 = tf.get_variable( 'b_8',shape=[ hidden_8 ],initializer=tf.contrib.layers.xavier_initializer())
				
				# Capa 9
				W_9 = tf.get_variable( 'W_9',shape=[ hidden_8, hidden_9 ],initializer=tf.contrib.layers.xavier_initializer())
				b_9 = tf.get_variable( 'b_9',shape=[ hidden_9 ],initializer=tf.contrib.layers.xavier_initializer())
				
				# Capa 10
				W_10 = tf.get_variable( 'W_10',shape=[ hidden_9, hidden_10 ],initializer=tf.contrib.layers.xavier_initializer())
				b_10 = tf.get_variable( 'b_10',shape=[ hidden_10 ],initializer=tf.contrib.layers.xavier_initializer())
				
				# Capa 11
				W_11 = tf.get_variable( 'W_11',shape=[ hidden_10, hidden_11 ],initializer=tf.contrib.layers.xavier_initializer())
				b_11 = tf.get_variable( 'b_11',shape=[ hidden_11 ],initializer=tf.contrib.layers.xavier_initializer())
				
				# FCf
				W_FCf = tf.get_variable( 'W_FCf',shape=[ hidden_11, hidden_FCf ],initializer=tf.contrib.layers.xavier_initializer())
				b_FCf = tf.get_variable( 'b_FCf',shape=[ hidden_FCf ],initializer=tf.contrib.layers.xavier_initializer())
			else:
				# Capa 1
				W_1_1 = tf.Variable(tf.random_normal([ batch_size, hidden_1 ], stddev = W_desv), dtype = tf.float32)
				b_1_1 = tf.Variable(tf.random_normal(shape = [ hidden_1 ], stddev = b_desv), dtype = tf.float32)
				W_1_2 = tf.Variable(tf.random_normal([ batch_size, hidden_1 ], stddev = W_desv), dtype = tf.float32)
				b_1_2 = tf.Variable(tf.random_normal(shape = [ hidden_1 ], stddev = b_desv), dtype = tf.float32)
				W_1_3 = tf.Variable(tf.random_normal([ batch_size, hidden_1 ], stddev = W_desv), dtype = tf.float32)
				b_1_3 = tf.Variable(tf.random_normal(shape = [ hidden_1 ], stddev = b_desv), dtype = tf.float32)
				
				# Capa 2
				W_2 = tf.Variable(tf.random_normal([ hidden_1, hidden_2 ], stddev = W_desv), dtype = tf.float32)
				b_2 = tf.Variable(tf.random_normal(shape = [ hidden_2 ], stddev = b_desv), dtype = tf.float32)
				
				# Capa 3
				W_3 = tf.Variable(tf.random_normal([ hidden_2, hidden_3 ], stddev = W_desv), dtype = tf.float32)
				b_3 = tf.Variable(tf.random_normal(shape = [ hidden_3 ], stddev = b_desv), dtype = tf.float32)
				
				# Capa 4
				W_4 = tf.Variable(tf.random_normal([ hidden_3, hidden_4 ], stddev = W_desv), dtype = tf.float32)
				b_4 = tf.Variable(tf.random_normal(shape = [ hidden_4 ], stddev = b_desv), dtype = tf.float32)
				
				# Capa 5
				W_5 = tf.Variable(tf.random_normal([ hidden_4, hidden_5 ], stddev = W_desv), dtype = tf.float32)
				b_5 = tf.Variable(tf.random_normal(shape = [ hidden_5 ], stddev = b_desv), dtype = tf.float32)
				
				# Capa 6
				W_6 = tf.Variable(tf.random_normal([ hidden_5, hidden_6 ], stddev = W_desv), dtype = tf.float32)
				b_6 = tf.Variable(tf.random_normal(shape = [ hidden_6 ], stddev = b_desv), dtype = tf.float32)
				
				# Capa 7
				W_7 = tf.Variable(tf.random_normal([ hidden_6, hidden_7 ], stddev = W_desv), dtype = tf.float32)
				b_7 = tf.Variable(tf.random_normal(shape = [ hidden_7 ], stddev = b_desv), dtype = tf.float32)
				
				# Capa 8
				W_8 = tf.Variable(tf.random_normal([ hidden_7, hidden_8 ], stddev = W_desv), dtype = tf.float32)
				b_8 = tf.Variable(tf.random_normal(shape = [ hidden_8 ], stddev = b_desv), dtype = tf.float32)
				
				# Capa 9
				W_9 = tf.Variable(tf.random_normal([ hidden_8, hidden_9 ], stddev = W_desv), dtype = tf.float32)
				b_9 = tf.Variable(tf.random_normal(shape = [ hidden_9 ], stddev = b_desv), dtype = tf.float32)
				
				# Capa 10
				W_10 = tf.Variable(tf.random_normal([ hidden_9, hidden_10 ], stddev = W_desv), dtype = tf.float32)
				b_10 = tf.Variable(tf.random_normal(shape = [ hidden_10 ], stddev = b_desv), dtype = tf.float32)
				
				# Capa 11
				W_11 = tf.Variable(tf.random_normal([ hidden_10, hidden_11 ], stddev = W_desv), dtype = tf.float32)
				b_11 = tf.Variable(tf.random_normal(shape = [ hidden_11 ], stddev = b_desv), dtype = tf.float32)
				
				# FCf
				W_FCf = tf.Variable(tf.random_normal([ hidden_11, hidden_FCf ], stddev = W_desv), dtype = tf.float32)
				b_FCf = tf.Variable(tf.random_normal(shape = [ hidden_FCf ], stddev = b_desv), dtype = tf.float32)
			
			## Capa 1
			Capa_1_1 = tf.matmul(Train_data_ph, W_1_1) + b_1_1
			
			Capa_1_2 = tf.matmul(Train_data_ph, W_1_2) + b_1_2
			
			Capa_1_3 = tf.matmul(Train_data_ph, W_1_3) + b_1_3
			
			Capa_1 = tf.identity(Capa_1_3 + Capa_1_2 + Capa_1_1)
			Capa_1 = tf.contrib.layers.batch_norm(Capa_1, is_training = bn_train, updates_collections = None)
			Capa_1 = tf.nn.relu(Capa_1)
			
			## Capa 2
			Capa_2 = tf.matmul(Capa_1, W_2) + b_2
			Capa_2 = tf.contrib.layers.batch_norm(Capa_2, is_training = bn_train, updates_collections = None)
			Capa_2 = tf.nn.relu(Capa_2)
			
			Capa_3 = tf.matmul(Capa_2, W_3) + b_3
			Capa_3 = tf.contrib.layers.batch_norm(Capa_3, is_training = bn_train, updates_collections = None)
			Capa_3 = tf.nn.relu(Capa_3)
			
			Capa_4 = tf.matmul(Capa_3, W_4) + b_4
			Capa_4 = tf.contrib.layers.batch_norm(Capa_4, is_training = bn_train, updates_collections = None)
			Capa_4 = tf.nn.relu(Capa_4)
			
			Capa_5 = tf.matmul(Capa_4, W_5) + b_5
			Capa_5 = tf.contrib.layers.batch_norm(Capa_5, is_training = bn_train, updates_collections = None)
			Capa_5 = tf.nn.relu(Capa_5)
			
			Capa_6 = tf.matmul(Capa_5, W_6) + b_6
			Capa_6 = tf.contrib.layers.batch_norm(Capa_6, is_training = bn_train, updates_collections = None)
			Capa_6 = tf.nn.relu(Capa_6)
			
			Capa_7 = tf.matmul(Capa_6, W_7) + b_7
			Capa_7 = tf.contrib.layers.batch_norm(Capa_7, is_training = bn_train, updates_collections = None)
			Capa_7 = tf.nn.relu(Capa_7)
			
			Capa_8 = tf.matmul(Capa_7, W_8) + b_8
			Capa_8 = tf.contrib.layers.batch_norm(Capa_8, is_training = bn_train, updates_collections = None)
			Capa_8 = tf.nn.relu(Capa_8)
			
			Capa_9 = tf.matmul(Capa_8, W_9) + b_9
			Capa_9 = tf.contrib.layers.batch_norm(Capa_9, is_training = bn_train, updates_collections = None)
			Capa_9 = tf.nn.relu(Capa_9)
			
			Capa_10 = tf.matmul(Capa_9, W_10) + b_10
			Capa_10 = tf.contrib.layers.batch_norm(Capa_10, is_training = bn_train, updates_collections = None)
			Capa_10 = tf.nn.relu(Capa_10)
			
			Capa_11 = tf.matmul(Capa_10, W_11) + b_11
			Capa_11 = tf.contrib.layers.batch_norm(Capa_11, is_training = bn_train, updates_collections = None)
			Capa_11 = tf.nn.relu(Capa_11)
			
			## Capa Final
			Capa_FCf = tf.matmul(Capa_11, W_FCf) + b_FCf
			
			prediction = Capa_FCf
			loss = (0.5)*(tf.norm(prediction-Original_data_ph))**2
			MAE = tf.reduce_mean(tf.abs(prediction - Original_data_ph))
			MAPE = tf.reduce_mean(tf.abs((prediction - Original_data_ph)/Original_data_ph))
			
			regularizer = 0
			regularizer = regularizer + tf.nn.l2_loss(W_1_1) + tf.nn.l2_loss(b_1_1)
			regularizer = regularizer + tf.nn.l2_loss(W_1_2) + tf.nn.l2_loss(b_1_2)
			regularizer = regularizer + tf.nn.l2_loss(W_1_3) + tf.nn.l2_loss(b_1_3)
			regularizer = regularizer + tf.nn.l2_loss(W_2) + tf.nn.l2_loss(b_2)
			regularizer = regularizer + tf.nn.l2_loss(W_3) + tf.nn.l2_loss(b_3)
			regularizer = regularizer + tf.nn.l2_loss(W_4) + tf.nn.l2_loss(b_4)
			regularizer = regularizer + tf.nn.l2_loss(W_5) + tf.nn.l2_loss(b_5)
			regularizer = regularizer + tf.nn.l2_loss(W_6) + tf.nn.l2_loss(b_6)
			regularizer = regularizer + tf.nn.l2_loss(W_7) + tf.nn.l2_loss(b_7)
			regularizer = regularizer + tf.nn.l2_loss(W_8) + tf.nn.l2_loss(b_8)
			regularizer = regularizer + tf.nn.l2_loss(W_9) + tf.nn.l2_loss(b_9)
			regularizer = regularizer + tf.nn.l2_loss(W_10) + tf.nn.l2_loss(b_10)
			regularizer = regularizer + tf.nn.l2_loss(W_11) + tf.nn.l2_loss(b_11)
			regularizer = regularizer + tf.nn.l2_loss(W_FCf) + tf.nn.l2_loss(b_FCf)
			
			loss_b = loss + beta*regularizer
			
			# Optimizer GradientDescent + Gradiente capado ( v1.1 )
			global_step_lr = tf.Variable(0, dtype = tf.float32)
			learning_rate = tf.train.exponential_decay(start_learning_rate, global_step_lr, stair_steps_learning_rate, multiply_learning_rate, staircase=True)
			optimizer = tf.train.GradientDescentOptimizer(learning_rate)
			
			gradients = optimizer.compute_gradients(loss_b)
			def ClipIfNotNone(grad):
				if grad is None:
					return grad
				return tf.clip_by_value(grad, min_gradient, max_gradient)
			
			clipped_gradients = [(ClipIfNotNone(grad), var) for grad, var in gradients]
			train = optimizer.apply_gradients(clipped_gradients, global_step=global_step_lr)
			
			correct_prediction = tf.equal( tf.round( prediction ), tf.round( Original_data_ph ) )
			accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
			
		
		# Feeder Train
		with tf.Session(graph=graph) as sess:
			tf.global_variables_initializer().run()
			
			# Plot 
			Sensor_to_plot = 5
			Var_to_plot = 1 # [0,1,2] "Intensity, Speed, Density"
			
			#########
			# TRAIN & VALIDATION
			#########
			
			Total_loss_train = np.zeros((num_epochs))
			Total_loss_val = np.zeros((int((num_epochs)/epoch_val)))
			val_epoch = 0
			
			# TRAIN var
			Epoch_loss_train = np.zeros((num_steps))
			Epoch_mae_train = np.zeros((num_steps))
			Epoch_mape_train = np.zeros((num_steps))
			Epoch_accuracy_train = np.zeros((num_steps))
			
			# VALIDATION var
			Epoch_loss_val = np.zeros((num_steps_val))
			Epoch_mae_val = np.zeros((num_steps_val))
			Epoch_mape_val = np.zeros((num_steps_val))
			Epoch_accuracy_val = np.zeros((num_steps_val))
			
			print(' \n \n-- -- Running MLP -- -- \n \n ')
			for epoch in range(num_epochs):
				original_data = np.zeros((min_sensores,1))
				
				
				print(' -- Epoch ', epoch)
				
				print(' - TRAIN - ')
				
				#for step in tqdm(range(num_steps)):
				for step in tqdm(np.random.permutation(num_steps)):
					try:
						offset = (step*batch_size) % (min_horas_train - batch_size)
						
						batch_data = Data_train[:,offset:offset+batch_size,Var_to_plot]
						original_data[:,0] = Data_train_original[:,offset+batch_size+pred_hours,Var_to_plot]
						
						feed_dict = {Train_data_ph : batch_data, Original_data_ph : original_data}
						_,loss_train, MAE_train, MAPE_train, acc_train = sess.run([train, loss, MAE, MAPE, accuracy], feed_dict = feed_dict)
						
						Epoch_loss_train[step] = np.mean(loss_train)
						Epoch_mae_train[step] = MAE_train
						Epoch_mape_train[step] = MAPE_train*100
						Epoch_accuracy_train[step] = acc_train*100
						
						time.sleep(0.0001)
					except KeyboardInterrupt:
						break
				Total_loss_train[epoch] = np.mean(Epoch_loss_train)
				
				print(" - Loss ", np.mean(Epoch_loss_train))
				print(" - MAE ", np.mean(Epoch_mae_train))
				
				if((epoch-1)%epoch_val==0)and(epoch!=0):
					
					print(' - VALIDATION - ')
					
					#for step in tqdm(range(num_steps_val)):
					for step in tqdm(np.random.permutation(num_steps_val)):
						try:
							offset = (step*batch_size) % (min_horas_val - batch_size)
							
							batch_data = Data_val[:,offset:offset+batch_size,Var_to_plot]
							original_data[:,0] = Data_val_original[:,offset+batch_size+pred_hours,Var_to_plot]
							
							feed_dict = {Train_data_ph : batch_data, Original_data_ph : original_data}
							loss_val, MAE_val, MAPE_val, acc_val = sess.run([loss, MAE, MAPE, accuracy], feed_dict = feed_dict)
							
							Epoch_loss_val[step] = np.mean(loss_val)
							Epoch_mae_val[step] = MAE_val
							Epoch_mape_val[step] = MAPE_val*100
							Epoch_accuracy_val[step] = acc_val*100
							
							time.sleep(0.0001)
						except KeyboardInterrupt:print(' - VALIDATION - ')
					Total_loss_val[val_epoch] = np.mean(Epoch_loss_val)
					val_epoch += 1
				
			
			print(" --- TRAIN ---")
			print(" - Last epoch MAE ", np.mean(Epoch_mae_train))
			print(" - Last epoch MAPE ", np.mean(Epoch_mape_train))
			print(' - Last epoch accuracy ' , np.mean(Epoch_accuracy_train))
			print(' - Last epoch loss ' , np.mean(Epoch_loss_train))
			print(' - Total train loss ' , np.mean(Total_loss_train))
			
			print(" --- VALIDATION ---")
			print(" - Last epoch MAE ", np.mean(Epoch_mae_val))
			print(" - Last epoch MAPE ", np.mean(Epoch_mape_val))
			print(' - Last epoch accuracy ' , np.mean(Epoch_accuracy_val))
			print(' - Last epoch loss ' , np.mean(Epoch_loss_val))
			print(' - Total validation loss ' , np.mean(Total_loss_val))
			
			#########
			# To PLOT Loss
			#########
			
			if True:
				
				plt.figure()
				Plot_total_loss = './plots/MLP_Total_loss.png'
				
				train = Total_loss_train
				val = Total_loss_val
				n = int(len(train)/len(val))
				
				plt.plot(train, label = 'Training loss')
				plt.plot([x + n - 1 for x in range(0, len(train), n)], val, '--', label	= 'Validation loss')
				plt.legend(fontsize=6)
				plt.ylabel('Loss')
				plt.xlabel('Epocas')
				#plt.show()
				plt.savefig(Plot_total_loss, dpi = 800)
			
			#########
			# To TXT
			#########
			
			MAE_train_txt = np.reshape(np.mean(Epoch_mae_train),(1,1))
			MAPE_train_txt = np.reshape(np.mean(Epoch_mape_train),(1,1))
			Total_loss_train_txt = np.reshape(np.mean(Total_loss_train),(1,1))
			Total_acc_train_txt = np.reshape(np.mean(Epoch_accuracy_train),(1,1))
			
			np.savetxt("./plots/MLP_MAE_train.txt",MAE_train_txt, delimiter='\n')
			np.savetxt("./plots/MLP_MAPE_train.txt",MAPE_train_txt, delimiter='\n')
			np.savetxt("./plots/MLP_Last_Total_Loss_train.txt",Total_loss_train_txt, delimiter='\n')
			np.savetxt("./plots/MLP_Last_Total_acc_train.txt",Total_acc_train_txt, delimiter='\n')
			
			MAE_val_txt = np.reshape(np.mean(Epoch_mae_val),(1,1))
			MAPE_val_txt = np.reshape(np.mean(Epoch_mape_val),(1,1))
			Total_loss_val_txt = np.reshape(np.mean(Total_loss_val),(1,1))
			Total_acc_val_txt = np.reshape(np.mean(Epoch_accuracy_val),(1,1))
			
			np.savetxt("./plots/MLP_MAE_val.txt",MAE_val_txt, delimiter='\n')
			np.savetxt("./plots/MLP_MAPE_val.txt",MAPE_val_txt, delimiter='\n')
			np.savetxt("./plots/MLP_Last_Total_loss_val.txt",Total_loss_val_txt, delimiter='\n')
			np.savetxt("./plots/MLP_Last_Total_acc_val.txt",Total_acc_val_txt, delimiter='\n')
			
			#########
			# TEST 
			#########
			
			if True:
				# Plot 
				Sensor_to_plot = 5
				Var_to_plot = 1 # [0,1,2] "Intensity, Speed, Density"
				
				original_data = np.zeros((min_sensores,1))
				Epoch_pred_test = np.zeros((min_sensores, num_steps_test))
				
				Epoch_loss_test = np.zeros((num_steps_test_b))
				Epoch_mae_test = np.zeros((num_steps_test_b))
				Epoch_mape_test = np.zeros((num_steps_test_b))
				Epoch_accuracy_test = np.zeros((num_steps_test_b))
				
				print(' - TESTING - ')
				
				for step in tqdm(range(num_steps_test)):
				#for step in tqdm(np.random.permutation(num_steps_val)):
					try:
						#offset = (step*batch_size) % (min_horas_test - batch_size)
						offset = step+batch_size
						
						batch_data = Data_test[:,step:offset,Var_to_plot]
						original_data[:,0] = Data_test_original[:,offset+pred_hours,Var_to_plot]
						
						feed_dict = {Train_data_ph : batch_data, Original_data_ph : original_data}
						loss_test, MAE_test, MAPE_test, acc_test, pred_test = sess.run([loss, MAE, MAPE, accuracy, prediction], feed_dict = feed_dict)
						
						Epoch_pred_test[:,step] = pred_test[:,0]
						
						time.sleep(0.0001)
					except KeyboardInterrupt:
						break
				
				#for step in tqdm(range(num_steps_test_b)):
				for step in tqdm(np.random.permutation(num_steps_test_b)):
					try:
						offset = (step*batch_size) % (min_horas_test - batch_size)
						#offset = step+batch_size
						
						batch_data = Data_test[:,offset:offset+batch_size,Var_to_plot]
						original_data[:,0] = Data_test_original[:,offset+batch_size+pred_hours,Var_to_plot]
						
						feed_dict = {Train_data_ph : batch_data, Original_data_ph : original_data}
						loss_test, MAE_test, MAPE_test, acc_test = sess.run([loss, MAE, MAPE, accuracy], feed_dict = feed_dict)
						
						Epoch_loss_test[step] = np.mean(loss_test)
						Epoch_mae_test[step] = MAE_test
						Epoch_mape_test[step] = MAPE_test*100
						Epoch_accuracy_test[step] = acc_test*100
						
						time.sleep(0.0001)
					except KeyboardInterrupt:
						break
				
				print(" --- Testing ---")
				print(" - Epoch MAE ", np.mean(Epoch_mae_test))
				print(" - Epoch MAPE ", np.mean(Epoch_mape_test))
				print(' - Epoch accuracy ' , np.mean(Epoch_accuracy_test))
				print(' - Epoch loss ' , np.mean(Epoch_loss_test))
				
				#########
				# To PLOT Prediction
				#########
				
				if True:
					datemin = datetime.datetime(2013, 1, 1, 0, 0)
					datemax = datetime.datetime(2013, 12, 31, 23, 0)
					plot_save_PRED = './plots/MLP_Test_PRED.png'
					plt.figure(figsize=(1280, 1280))
					y = Data_test_original[Sensor_to_plot,:,Var_to_plot]
					x = [datemin + datetime.timedelta(minutes=i*5) for i in range(len(y))]
					y_p = Epoch_pred_test[Sensor_to_plot, :]
					y = y[76275+batch_size+pred_hours:77727+batch_size+pred_hours]
					x = x[76275+batch_size+pred_hours:77727+batch_size+pred_hours]
					y_p = y_p[76275:77727]
					plt.xticks(x, rotation='vertical',fontsize=6)
					plt.gca().xaxis.set_major_formatter(mdates.DateFormatter('%A - %H:%M'))
					plt.gca().xaxis.set_major_locator(mdates.HourLocator(interval = 2))
					plt.gcf().autofmt_xdate()
					plt.ylabel('Velocidad en el sensor [km/h]',fontsize=12)
					plt.xlabel('Dia - Hora',fontsize=12)
					plt.plot(x,y, label='Velocidad original')
					plt.plot(x,y_p, label = 'Velocidad predicha')
					plt.legend(fontsize=6)
					plt.tight_layout()
				#	plt.show()
					plt.savefig(plot_save_PRED,dpi=800)
				
				#########
				# To PLOT Loss
				#########
				
				if False:
					
					plt.figure()
					Plot_total_loss = './plots/MLP_Epoch_loss.png'
					
					train = Epoch_loss_train
					val = Epoch_loss_val
					test = Epoch_loss_test
					
					n = int(len(train)/len(val))
					m = int(len(train)/len(test))
					
					plt.plot(train)
					plt.plot([x + n - 1 for x in range(0, len(train), n)], val, '--')
					plt.plot([x + m - 1 for x in range(0, len(train), m)], test, '-.', color = 'red')
					#plt.show()
					plt.savefig(Plot_total_loss, dpi = 800)
				
				#########
				# To TXT
				#########
				
				if True:
					MAE_test_txt = np.reshape(np.mean(Epoch_mae_test),(1,1))
					MAPE_test_txt = np.reshape(np.mean(Epoch_mape_test),(1,1))
					Total_acc_test_txt = np.reshape(np.mean(Epoch_accuracy_test),(1,1))
					Total_loss_test_txt = np.reshape(np.mean(Epoch_loss_test),(1,1))
					
					np.savetxt("./plots/MLP_MAE_test.txt",MAE_test_txt, delimiter='\n')
					np.savetxt("./plots/MLP_MAPE_test.txt",MAPE_test_txt, delimiter='\n')
					np.savetxt("./plots/MLP_Last_Total_acc_test.txt",Total_acc_test_txt, delimiter='\n')
					np.savetxt("./plots/MLP_Last_Total_Loss_test.txt",Total_loss_test_txt, delimiter='\n')
			
			
	except ValueError as Error:
		print(" -- Error : ", Error)
	return 




